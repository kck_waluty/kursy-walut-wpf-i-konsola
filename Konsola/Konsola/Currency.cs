﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixerSharp;
using System.Threading;

namespace Konsola
{
    static class Currency
    {
        public class CurrencyPrice
        {
            public string Name { get; set; }
            public double Value { get; set; }
            public double PercentDifference { get; set; }
            public double PriceDifference { get; set; }

            public CurrencyPrice(string name, double value, double percentDifference=0, double priceDifference=0)
            {
                this.Name = name;
                this.Value = value;
                this.PercentDifference = percentDifference;
                this.PriceDifference = priceDifference;
            }

        }

        public static string[] symbols = new string[]
        {
            "AUD",
            "BGN",
            "BRL",
            "CAD",
            "CHF",
            "CNY",
            "CZK",
            "DKK",
            "EUR",
            "GBP",
            "HKD",
            "HRK",
            "HUF",
            //"IDR",
            "ILS",
            "INR",
            "JPY",
            "KRW",
            "MXN",
            "MYR",
            "NOK",
            "NZD",
            "PHP",
            //"PLN",
            "RON",
            "RUB",
            "SEK",
            "SGD",
            "THB",
            "TRY",
            "USD",
            "ZAR"
        };

        public static string[] symbolsForCalculator = new string[]
        {
            "AUD",
            "BGN",
            "BRL",
            "CAD",
            "CHF",
            "CNY",
            "CZK",
            "DKK",
            "EUR",
            "GBP",
            "HKD",
            "HRK",
            "HUF",
            //"IDR",
            "ILS",
            "INR",
            "JPY",
            "KRW",
            "MXN",
            "MYR",
            "NOK",
            "NZD",
            "PHP",
            "PLN",
            "RON",
            "RUB",
            "SEK",
            "SGD",
            "THB",
            "TRY",
            "USD",
            "ZAR"
        };

        public static double convert(string from, string to, double amount, DateTime date)
        {
            return Fixer.Convert(from, to, amount, date);
        }

        public static List<CurrencyPrice> getPricesOnDate(DateTime date)
        {
            List<CurrencyPrice> list = new List<CurrencyPrice>();

            foreach(string c in symbols)
            {
                double price = convert(c, "PLN", 1, date);
                if (!Double.IsInfinity(price) && !Double.IsNaN(price))
                {
                    double price7DaysEarlier = convert(c, "PLN", 1, date.AddDays(-7));
                    double percentDifference = (price - price7DaysEarlier) / price7DaysEarlier * 100;

                    //percentDifference = Math.Round(percentDifference, 3);

                    list.Add(new CurrencyPrice(c, price, percentDifference, price - price7DaysEarlier));
                }                  
            }

            return list;
        }

        public async static Task<List<CurrencyPrice>> getPricesOnDateAsync(DateTime date)
        {
            return await Task.Run(() => getPricesOnDate(date));
        }

        public static Dictionary<DateTime, CurrencyPrice> getPricesUntilDate(string currency, DateTime dateTo, int numberOfDaysBack = 7)
        {
            Dictionary<DateTime, CurrencyPrice> dictionary = new Dictionary<DateTime, CurrencyPrice>();

            for(int i=0; i >= -numberOfDaysBack; i--)
            {
                DateTime date = dateTo.AddDays(i);
                double price = convert(currency, "PLN", 1, date);
                if (double.IsInfinity(price))
                {
                    dictionary.Add(date, new CurrencyPrice(currency, 0, 0, 0));
                    continue;
                }
                double price7DaysEarlier = Fixer.Convert(currency, "PLN", 1, date.AddDays(-7));
                double percentDifference = (price - price7DaysEarlier) / price7DaysEarlier * 100;

                //percentDifference = Math.Round(percentDifference, 3);

                dictionary.Add(date, new CurrencyPrice(currency, price, percentDifference, price - price7DaysEarlier));

            }
            return dictionary;
        }

        public static Dictionary<DateTime, CurrencyPrice> getPricesUntilDateToGraph(string currency, DateTime dateTo, int numberOfDaysBack = 7)
        {
            Dictionary<DateTime, CurrencyPrice> dictionary = new Dictionary<DateTime, CurrencyPrice>();

            for (int i = 0; i >= -numberOfDaysBack; i--)
            {
                DateTime date = dateTo.AddDays(i);
                double price = convert(currency, "PLN", 1, date);
                if (double.IsInfinity(price))
                {
                    dictionary.Add(date, new CurrencyPrice(currency, 0, 0, 0));
                    continue;
                }

                price = Math.Round(price, 3);

                dictionary.Add(date, new CurrencyPrice(currency, price));
            }
            return dictionary;
        }

        public static Task<Dictionary<DateTime, CurrencyPrice>> getPricesUntilDateAsync(string currency, DateTime dateTo, int numberOfDaysBack = 7)
        {
            return Task.Run(() => getPricesUntilDate(currency, dateTo, numberOfDaysBack));
        }

        public static Task<Dictionary<DateTime, CurrencyPrice>> getPricesUntilDateToGraphAsync(string currency, DateTime dateTo, int numberOfDaysBack = 7)
        {
            return Task.Run(() => getPricesUntilDateToGraph(currency, dateTo, numberOfDaysBack));
        }
    }
}
