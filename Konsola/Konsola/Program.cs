﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Globalization;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Konsola
{
    class Program
    {
        const char BLANK = ' ';
        const char DOT = '┬';
        const char X = '.';

        private static void InitalizeLogo(int intline)
        {
            ConsoleColor[] colors = (ConsoleColor[])ConsoleColor.GetValues(typeof(ConsoleColor));
            colors.SetValue(ConsoleColor.Cyan, 0);
            int lengthColors = colors.Length;
            int i = 0;
            Console.ResetColor();
            Console.CursorVisible = false;

            string[] lines = System.IO.File.ReadAllLines("sym_euro.txt");
            System.Console.WriteLine();
            System.Console.WriteLine();
            int tempWidth = Console.WindowWidth;
            foreach (string line in lines)
            {
                Console.SetCursorPosition((tempWidth - line.Length) / 2, intline++);
                Console.CursorVisible = false;
                Console.ForegroundColor = colors[i++ % lengthColors];
                Console.WriteLine(line);
                Console.Beep(400, 100);
            }

            intline += 2;
            Console.SetCursorPosition((Console.WindowWidth - "Aby kontynuwoac, nacisnij dowolny przycisk...".Length) / 2, intline);
            Console.WriteLine("Aby kontynuwoac, nacisnij dowolny przycisk...");
            System.Console.ReadKey(true);
            new Thread(() => Console.Beep(500, 200)).Start();

            Console.CursorVisible = false;
        }

        static void Main(string[] args)
        {
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;

            int selectedItem = 0;
            int selectedColor = 0;

            string[] menuItems = { " Zestawienie kursu walut ", " Kalkulator walut ", " Wygeneruj wykres ", " Wyjście z programu " };

            int logo = 0;
            bool start = false;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    if (logo == 0)
                    {
                        start = false;
                        InitalizeLogo(line);
                        logo++;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        string s = "      ─────────────── MENU ───────────────      ";
                        Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                        Console.WriteLine(s);
                        Console.ForegroundColor = ConsoleColor.White;


                        Console.WriteLine(""); line += 2;
                        position_y = line;
                    }
                }

                ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

                if (logo == 2)
                {
                    for (int i = 0; i < menuItems.Length; i++)
                    {
                        if (selectedItem == i)
                        {
                            Console.BackgroundColor = color[selectedColor++ % 5];
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length) / 2, i + position_y);
                            Console.Write(menuItems[i]);
                            Console.ResetColor();
                            Console.Write(" ");
                        }
                        else
                        {
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length) / 2, i + position_y);
                            Console.WriteLine(menuItems[i]);
                        }
                    }
                }
                else if (logo == 1)
                {
                    logo++;
                }

                Thread.Sleep(300);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.UpArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if ((selectedItem - 1) < 0)
                            {
                                selectedItem = menuItems.Length - 1;
                            }
                            else
                            {
                                selectedItem--;
                            }

                            break;

                        case ConsoleKey.DownArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if ((selectedItem + 1) > menuItems.Length - 1)
                            {
                                selectedItem = 0;
                            }
                            else
                            {
                                selectedItem++;
                            }
                            break;


                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if (selectedItem == 3)
                            {
                                Environment.Exit(0);
                            }
                            else if (selectedItem == 2)
                            {
                                ChooseGraphOptions();
                            }
                            else if (selectedItem == 1)
                            {
                                CalucatorCurrency();
                            }
                            else if (selectedItem == 0)
                            {
                                CurrencyExchange();
                            }
                            start = false;
                            break;
                    }
                }

            }

        }

        private static void InitializeProgramLogo(int line)
        {
            string s = "╔═══════════════════════════════════════════╗";
            Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
            Console.Write(s);
            Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
            Console.Write("║");
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.Write("                                           ");
            Console.ResetColor();
            Console.Write("║");
            Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
            Console.Write("║");
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.Write("     APLIKACJA DO PRZEGLĄDU KURSU WALUT    ");
            Console.ResetColor();
            Console.Write("║");
            Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
            Console.Write("║");
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.Write("                                           ");
            Console.ResetColor();
            Console.Write("║");
            Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
            Console.WriteLine("╚═══════════════════════════════════════════╝");
            Console.WriteLine("");
            Console.WriteLine("");
        }

        private static void ValidateDate(DateTime date, int addDays = 0)
        {
            DateTime oldestValidDate = new DateTime(1999, 1, 4).AddDays(addDays);

            if (date < oldestValidDate || date > DateTime.Now)
                throw new ApplicationException("Not valid date.");
        }

        private static void CalucatorCurrency()
        {
            int selectedItem = 0;
            int selectedColor = 0;
            string currentCurrency = SelectCurrencyForCalculator(selectedItem, selectedColor, "               PRZELICZ Z:                ", "      ─────────────── KALKULATOR WALUT ───────────────      ");
            string foreignCurrency = SelectCurrencyForCalculator(selectedItem, selectedColor, "               PRZELICZ NA:               ", "      ─────────────── KALKULATOR WALUT ───────────────      ");
            DateTime dateCurrency;
            string stringCurrentDate = DateTime.Now.ToString("dd.MM.yyyy");
            double amount;
            double resume;

            if (currentCurrency == foreignCurrency)
            {
                ShowErrorTheSameCurrency(selectedColor);
            }
            else
            {
                try
                {
                    stringCurrentDate = SelectCurrencyDate(stringCurrentDate, "─────────────── WYKRES NOTOWANIA WALUT ───────────────");
                    if (DateTime.TryParseExact(stringCurrentDate, "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                    else if (DateTime.TryParseExact(stringCurrentDate, "d.M.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                    else if (DateTime.TryParseExact(stringCurrentDate, "d.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                    else if (DateTime.TryParseExact(stringCurrentDate, "dd.M.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                    else { throw new Exception(); }
                    ValidateDate(dateCurrency);
                }
                catch (Exception e)
                {
                    ShowErrorBadDate(selectedColor);
                    return;
                }
                try
                {
                    amount = Double.Parse(SelectAmount(), CultureInfo.InvariantCulture);
                }
                catch (Exception e)
                {
                    ShowErrorBadAmount(selectedColor);
                    return;
                }
                try
                {
                    resume = Currency.convert(currentCurrency, foreignCurrency, amount, dateCurrency);

                }
                catch (Exception e)
                {
                    ShowErrorConnectToAPI(0);
                    return;
                }
                ShowCalculatedResume(resume, currentCurrency, foreignCurrency, amount);

            }
            Console.ResetColor();
        }

        private static string SelectCurrency(int selectedItem, int selectedColor, string name, string title)
        {
            ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };
            string[] menuItems = Currency.symbols;
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;

                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.SetCursorPosition((Console.WindowWidth - title.Length) / 2, line);
                    Console.WriteLine(title);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write(name);
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");

                    Console.WriteLine(""); line += 2;
                    position_y = Console.CursorTop;



                    for (int i = 0; i < menuItems.Length; i++)
                    {
                        if (selectedItem == i)
                        {
                            Console.BackgroundColor = color[0];
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length - 2) / 2, i + line);
                            Console.Write(" " + menuItems[i] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                        }
                        else
                        {
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length - 2) / 2, i + line);
                            Console.WriteLine(" " + menuItems[i] + " ");
                        }
                    }
                }

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.UpArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            int oldSelectedItem;
                            if ((selectedItem - 1) < 0)
                            {
                                oldSelectedItem = selectedItem;
                                selectedItem = menuItems.Length - 1;
                            }
                            else
                            {
                                oldSelectedItem = selectedItem;
                                selectedItem--;


                            }
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[oldSelectedItem].Length - 2) / 2, position_y + oldSelectedItem);
                            Console.Write(" " + menuItems[oldSelectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            Console.BackgroundColor = color[0];
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[selectedItem].Length - 2) / 2, position_y + selectedItem);
                            Console.Write(" " + menuItems[selectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            break;

                        case ConsoleKey.DownArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            int oldDownSelectedItem;
                            if ((selectedItem + 1) > menuItems.Length - 1)
                            {
                                oldDownSelectedItem = selectedItem;
                                selectedItem = 0;
                            }
                            else
                            {
                                oldDownSelectedItem = selectedItem;
                                selectedItem++;
                            }
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[oldDownSelectedItem].Length - 2) / 2, position_y + oldDownSelectedItem);
                            Console.Write(" " + menuItems[oldDownSelectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            Console.BackgroundColor = color[0];
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[selectedItem].Length - 2) / 2, position_y + selectedItem);
                            Console.Write(" " + menuItems[selectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            break;
                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();

                            return menuItems[selectedItem];
                    }

                }
            }
        }

        private static string SelectCurrencyForCalculator(int selectedItem, int selectedColor, string name, string title)
        {
            ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };
            string[] menuItems = Currency.symbolsForCalculator;
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;

                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.SetCursorPosition((Console.WindowWidth - title.Length) / 2, line);
                    Console.WriteLine(title);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write(name);
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");

                    Console.WriteLine(""); line += 2;
                    position_y = Console.CursorTop;



                    for (int i = 0; i < menuItems.Length; i++)
                    {
                        if (selectedItem == i)
                        {
                            Console.BackgroundColor = color[0];
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length - 2) / 2, i + line);
                            Console.Write(" " + menuItems[i] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                        }
                        else
                        {
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length - 2) / 2, i + line);
                            Console.WriteLine(" " + menuItems[i] + " ");
                        }
                    }
                }

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.UpArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            int oldSelectedItem;
                            if ((selectedItem - 1) < 0)
                            {
                                oldSelectedItem = selectedItem;
                                selectedItem = menuItems.Length - 1;
                            }
                            else
                            {
                                oldSelectedItem = selectedItem;
                                selectedItem--;


                            }
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[oldSelectedItem].Length - 2) / 2, position_y + oldSelectedItem);
                            Console.Write(" " + menuItems[oldSelectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            Console.BackgroundColor = color[0];
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[selectedItem].Length - 2) / 2, position_y + selectedItem);
                            Console.Write(" " + menuItems[selectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            break;

                        case ConsoleKey.DownArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            int oldDownSelectedItem;
                            if ((selectedItem + 1) > menuItems.Length - 1)
                            {
                                oldDownSelectedItem = selectedItem;
                                selectedItem = 0;
                            }
                            else
                            {
                                oldDownSelectedItem = selectedItem;
                                selectedItem++;
                            }
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[oldDownSelectedItem].Length - 2) / 2, position_y + oldDownSelectedItem);
                            Console.Write(" " + menuItems[oldDownSelectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            Console.BackgroundColor = color[0];
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - menuItems[selectedItem].Length - 2) / 2, position_y + selectedItem);
                            Console.Write(" " + menuItems[selectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            break;
                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();

                            return menuItems[selectedItem];
                    }

                }
            }
        }


        private static string SelectCurrencyDate(string date, string title)
        {
            int selectedColor = 0;
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.SetCursorPosition((Console.WindowWidth - title.Length) / 2, line);
                    Console.WriteLine(title);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("               DATA WALUTY                ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");
                    Console.ResetColor();




                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, line - 2);
                    Console.Write(" OK ");
                    Console.ResetColor();

                    position_y = line - 4;

                }

                Thread.Sleep(100);
                Console.CursorVisible = true;
                Console.SetCursorPosition((Console.WindowWidth - "                                            ".Length) / 2, position_y);
                Console.ResetColor();
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                                          ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - date.Length) / 2, position_y);
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write(date);

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.Backspace:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if (!String.IsNullOrEmpty(date))
                                date = date.Remove(date.Length - 1);
                            break;

                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            Console.CursorVisible = false;
                            return date;

                        default:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            for (int i = 0; i < 10; i++)
                            {
                                if (cki.KeyChar == '.' || cki.KeyChar == Char.Parse(i.ToString()))
                                {
                                    date += cki.KeyChar;
                                    break;
                                }
                            }

                            break;
                    }
                }
            }
            Console.ResetColor();
            Console.CursorVisible = false;
        }

        private static string SelectCurrencyDateForGraph(string date, string title)
        {
            int selectedColor = 0;
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.SetCursorPosition((Console.WindowWidth - title.Length) / 2, line);
                    Console.WriteLine(title);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("         DATA OSTATNIEGO NOTOWANIA        ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");
                    Console.ResetColor();




                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, line - 2);
                    Console.Write(" OK ");
                    Console.ResetColor();

                    position_y = line - 4;

                }

                Thread.Sleep(100);
                Console.CursorVisible = true;
                Console.SetCursorPosition((Console.WindowWidth - "                                            ".Length) / 2, position_y);
                Console.ResetColor();
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                                          ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - date.Length) / 2, position_y);
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write(date);

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.Backspace:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if (!String.IsNullOrEmpty(date))
                                date = date.Remove(date.Length - 1);
                            break;

                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            Console.CursorVisible = false;
                            return date;

                        default:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            for (int i = 0; i < 10; i++)
                            {
                                if (cki.KeyChar == '.' || cki.KeyChar == Char.Parse(i.ToString()))
                                {
                                    date += cki.KeyChar;
                                    break;
                                }
                            }

                            break;
                    }
                }
            }
            Console.ResetColor();
            Console.CursorVisible = false;
        }



        private static string SelectAmount()
        {
            int selectedColor = 0;

            string amount = "0";
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;
                    Console.CursorVisible = true;
                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── KALKULATOR WALUT ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("               PODAJ KWOTE                ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");
                    Console.ResetColor();

                    ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

                    Console.BackgroundColor = color[0];
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, line - 2);
                    Console.Write(" OK ");
                    Console.ResetColor();

                    position_y = line - 4;

                }

                Console.CursorVisible = true;
                Thread.Sleep(100);
                Console.SetCursorPosition((Console.WindowWidth - "                                            ".Length) / 2, position_y);
                Console.ResetColor();
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                                          ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - amount.Length) / 2, position_y);
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write(amount);

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.Backspace:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if (!String.IsNullOrEmpty(amount))
                                amount = amount.Remove(amount.Length - 1);
                            break;

                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            Console.CursorVisible = false;
                            return amount;

                        default:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            for (int i = 0; i < 10; i++)
                            {
                                if (cki.KeyChar == '.' || cki.KeyChar == Char.Parse(i.ToString()))
                                {
                                    amount += cki.KeyChar;
                                    break;
                                }
                            }

                            break;
                    }
                }
                Console.CursorVisible = false;
                Console.ResetColor();

            }
        }

        private static void ShowErrorTheSameCurrency(int selectedColor)
        {
            ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;
                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── KALKULATOR WALUT ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                  BLAD!                   ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("        PODALES TAKIE SAME WALUTY         ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("           SPROBUJ JESZCZE RAZ            ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");

                    position_y = line - 2;

                }
                Console.BackgroundColor = color[selectedColor++ % 5];
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, position_y);
                Console.Write(" OK ");
                Console.ResetColor();

                Thread.Sleep(500);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    if (cki.Key == ConsoleKey.Enter)
                    {
                        break;
                    }

                }
            }
        }

        private static void ShowErrorConnectToAPI(int selectedColor)
        {
            ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;
                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── KALKULATOR WALUT ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                  BLAD!                   ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("      PROBLEMY Z POLACZENIEM DO BAZY      ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("       SPROBUJ JESZCZE RAZ ZA CHWILE      ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");

                    position_y = line - 2;

                }
                Console.BackgroundColor = color[selectedColor++ % 5];
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, position_y);
                Console.Write(" OK ");
                Console.ResetColor();

                Thread.Sleep(500);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    if (cki.Key == ConsoleKey.Enter)
                    {
                        break;
                    }

                }
            }
        }

        private static void ShowErrorResolutionScreen(int selectedColor)
        {
            ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;
                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── KALKULATOR WALUT ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                  BLAD!                   ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("        PROBLEM Z WIELKOSCIA OKNA         ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("              POWIEKSZ OKNO               ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");

                    position_y = line - 2;

                }
                Console.BackgroundColor = color[selectedColor++ % 5];
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, position_y);
                Console.Write(" OK ");
                Console.ResetColor();

                Thread.Sleep(500);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    if (cki.Key == ConsoleKey.Enter)
                    {
                        break;
                    }

                }
            }
        }

        private static void ShowErrorBadDate(int selectedColor)
        {

            ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;
                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── KALKULATOR WALUT ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                  BLAD!                   ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("             PODALES ZLA DATE             ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("           SPROBUJ JESZCZE RAZ            ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");

                    position_y = line - 2;

                }


                Console.BackgroundColor = color[selectedColor++ % 5];
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, position_y);
                Console.Write(" OK ");
                Console.ResetColor();

                Thread.Sleep(500);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    if (cki.Key == ConsoleKey.Enter)
                    {
                        break;
                    }

                }
            }
        }

        private static void ShowErrorCreateGraph(int selectedColor)
        {

            ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;
                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── KALKULATOR WALUT ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                  BLAD!                   ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("             RYSOWANIA WYKRESU            ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("           PODAJ INNE PARAMETRY           ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");

                    position_y = line - 2;

                }


                Console.BackgroundColor = color[selectedColor++ % 5];
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, position_y);
                Console.Write(" OK ");
                Console.ResetColor();

                Thread.Sleep(500);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    if (cki.Key == ConsoleKey.Enter)
                    {
                        break;
                    }

                }
            }
        }

        private static void ShowErrorBadAmount(int selectedColor)
        {
            ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;
                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── KALKULATOR WALUT ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                  BLAD!                   ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("          PODALES ZLA WARTOSC             ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("           SPROBUJ JESZCZE RAZ            ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.WriteLine("└──────────────────────────────────────────┘");

                    position_y = line - 2;

                }

                Console.BackgroundColor = color[selectedColor++ % 5];
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, position_y);
                Console.Write(" OK ");
                Console.ResetColor();

                Thread.Sleep(500);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    if (cki.Key == ConsoleKey.Enter)
                    {
                        break;
                    }

                }
            }
        }

        private static void ShowCalculatedResume(double resume, string from, string to, double value)
        {
            int selectedColor = 0;
            while (true)
            {
                Console.ResetColor();
                Console.Clear();
                Console.CursorVisible = false;
                int line = 2;

                InitializeProgramLogo(line);
                line = 9;

                Console.ForegroundColor = ConsoleColor.Blue;
                string s = "      ─────────────── KALKULATOR WALUT ───────────────      ";
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                Console.WriteLine(s);
                Console.ResetColor();

                line += 2;

                s = "┌──────────────────────────────────────────┐";
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                Console.WriteLine(s);
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                                          ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                  WYNIK                   ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                                          ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                                          ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                                          ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                                          ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                Console.Write("│");
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("                                          ");
                Console.ResetColor();
                Console.Write("│");
                Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                Console.WriteLine("└──────────────────────────────────────────┘");

                ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

                string sResult = Math.Round(value, 2).ToString() + " " + from + " = " + Math.Round(resume, 2).ToString() + " " + to;
                Console.SetCursorPosition((Console.WindowWidth - sResult.Length) / 2, line - 4);
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write(sResult);
                Console.ResetColor();

                Console.BackgroundColor = color[selectedColor++ % 5];
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition((Console.WindowWidth - " OK ".Length) / 2, line - 2);
                Console.Write(" OK ");
                Console.ResetColor();

                Thread.Sleep(500);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    if (cki.Key == ConsoleKey.Enter)
                    {
                        break;
                    }

                }
            }
        }

        private static void CurrencyExchange()
        {
            int selectedItem = 0;
            int selectedColor = 0;
            DateTime dateCurrency;
            string stringCurrentDate = DateTime.Now.ToString("dd.MM.yyyy");
            decimal amount;

            try
            {
                stringCurrentDate = SelectCurrencyDate(stringCurrentDate, "─────────────── WYKRES NOTOWANIA WALUT ───────────────");
                if (DateTime.TryParseExact(stringCurrentDate, "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                else if (DateTime.TryParseExact(stringCurrentDate, "d.M.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                else if (DateTime.TryParseExact(stringCurrentDate, "d.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                else if (DateTime.TryParseExact(stringCurrentDate, "dd.M.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                else { throw new Exception(); }
                ValidateDate(dateCurrency);
            }
            catch (Exception e)
            {
                ShowErrorBadDate(selectedColor);
                return;
            }

            ShowCurrencyExchange(dateCurrency, selectedItem);

            Console.ResetColor();
        }

        private static int PrintRowInTheCurrencyList(List<Currency.CurrencyPrice> getPricesOnDate, ConsoleColor bgColor, ConsoleColor fgColor, int line, int i)
        {
            string s = "        │           │            │               ";

            Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
            Console.Write("│");
            Console.BackgroundColor = bgColor;
            Console.ForegroundColor = fgColor;
            Console.Write("        │           │            │             ");
            Console.ResetColor();
            Console.Write("│");
            Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
            Console.Write("│");
            Console.BackgroundColor = bgColor;
            //▲▼─
            Console.ForegroundColor = fgColor;
            Console.Write("  " + getPricesOnDate[i].Name + "   │");
            Console.Write("   " + String.Format(CultureInfo.InvariantCulture,
                "{0:0.000}", getPricesOnDate[i].Value) + "   │");
            if (getPricesOnDate[i].PercentDifference < 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("  " + "▼ " + String.Format(CultureInfo.InvariantCulture,
                "{0:0.000}", Math.Abs(getPricesOnDate[i].PercentDifference)) + "%");
            }
            else if (getPricesOnDate[i].PercentDifference > 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("  " + "▲ " + String.Format(CultureInfo.InvariantCulture, "{0:0.000}", getPricesOnDate[i].PercentDifference) + "%");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("  " + "─ " + String.Format(CultureInfo.InvariantCulture, "{0:0.000}", getPricesOnDate[i].PercentDifference) + "%");
            }
            Console.ForegroundColor = fgColor;
            Console.Write("  │");
            if (getPricesOnDate[i].PriceDifference < 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("   " + "▼ " + String.Format(CultureInfo.InvariantCulture,
                "{0:0.000}", Math.Abs(getPricesOnDate[i].PriceDifference)));
            }
            else if (getPricesOnDate[i].PriceDifference > 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("   " + "▲ " + String.Format(CultureInfo.InvariantCulture,
                "{0:0.000}", getPricesOnDate[i].PriceDifference));
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("   " + "─ " + String.Format(CultureInfo.InvariantCulture,
                "{0:0.000}", getPricesOnDate[i].PriceDifference));
            }
            Console.Write("   ");
            Console.ResetColor();
            Console.Write("│");
            Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
            Console.Write("│");
            Console.BackgroundColor = bgColor;
            Console.ForegroundColor = fgColor;
            Console.Write("        │           │            │             ");
            Console.ResetColor();
            Console.Write("│");

            return line;
        }

        private static string ShowCurrencyExchange(DateTime dateCurrency, int selectedItem)
        {
            int lineBack = 0;
            int selectedColor = 0;

            List<Currency.CurrencyPrice> getPricesOnDate = null;
            Console.BackgroundColor = ConsoleColor.Black;

            int width = Console.WindowWidth;
            int height = Console.WindowHeight;

            int saveSelectedItem = -1;
            int line = 2;
            bool start = false;
            bool list = false;
            string s;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;
                    saveSelectedItem = selectedItem;

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;

                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    line = 2;
                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── KURSY WALUT Z DNIA " + dateCurrency.ToString("dd.MM.yyyy") + " ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌───────────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.Write(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│ Waluta │ Kurs (zl) │ Zmiana (%) │ Zmiana (zł) │");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("└───────────────────────────────────────────────┘");

                    if (getPricesOnDate == null)
                    {

                        Console.SetCursorPosition((Console.WindowWidth - " Trwa ladowanie ".Length) / 2, 16);
                        Console.WriteLine(" Trwa ladowanie ");

                        var t1 = Task.Run(() =>
                        {
                            try
                            {
                                getPricesOnDate = Currency.getPricesOnDate(dateCurrency);
                            }
                            catch (Exception e)
                            {
                                //ShowErrorConnectToAPI(0);
                                return;
                            }
                        });

                        var t2 = Task.Run(() =>
                        {
                            using (var progress = new ProgressBar())
                            {
                                Console.SetCursorPosition((Console.WindowWidth - 18) / 2, 18);
                                for (int i = 0; i <= 100; i++)
                                {
                                    progress.Report((double)i / 100);
                                    Thread.Sleep(80);
                                }
                            }
                        });

                        Task.WaitAll(new[] { t1, t2 }, 20000);
                        Console.Beep(500, 400);


                    }

                    if (getPricesOnDate == null)
                    {
                        ShowErrorConnectToAPI(0);
                        return "Powrot";
                    }
                    if (!list)
                    {
                        selectedItem = getPricesOnDate.Count;
                        list = true;
                    }

                    s = "┌───────────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write(s);

                    ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

                    Console.WriteLine("");

                    for (int i = 0; i < getPricesOnDate.Count + 1; i++)
                    {
                        if (selectedItem == i)
                        {
                            if (selectedItem != getPricesOnDate.Count)
                            {
                                line = PrintRowInTheCurrencyList(getPricesOnDate, color[0], ConsoleColor.Black, line, i);
                            }
                            else
                            {
                                line += 2;
                                lineBack = line + 1;
                                Console.SetCursorPosition((Console.WindowWidth - " Powrot ".Length) / 2, ++line);
                                Console.BackgroundColor = color[0];
                                Console.ForegroundColor = ConsoleColor.Black;
                                Console.Write(" Powrot ");
                                Console.ResetColor();
                            }
                        }
                        else
                        {
                            if (i != getPricesOnDate.Count)
                            {
                                line = PrintRowInTheCurrencyList(getPricesOnDate, ConsoleColor.Black, color[0], line, i);
                            }
                            else
                            {
                                line += 2;
                                Console.SetCursorPosition((Console.WindowWidth - " Powrot ".Length) / 2, ++line);
                                lineBack = line;
                                Console.Write(" Powrot ");
                                Console.ResetColor();
                            }
                        }
                    }

                    line -= 2;

                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine("└───────────────────────────────────────────────┘");
                    Console.ResetColor();

                    //Thread.Sleep(500);
                }

                if (Console.KeyAvailable == true)
                {
                    line = selectedItem * 3 + 14;
                    s = "        │           │            │               ";

                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.UpArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();

                            if (selectedItem == getPricesOnDate.Count)
                            {
                                Console.SetCursorPosition((Console.WindowWidth - " Powrot ".Length) / 2, lineBack);
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.Gray;
                                Console.Write(" Powrot ");
                                Console.ResetColor();

                                line = PrintRowInTheCurrencyList(getPricesOnDate, ConsoleColor.Gray, ConsoleColor.Black, line - 3, --selectedItem);
                            }
                            else if (selectedItem > 0)
                            {
                                line = PrintRowInTheCurrencyList(getPricesOnDate, ConsoleColor.Gray, ConsoleColor.Black, line - 3, --selectedItem);
                                line = PrintRowInTheCurrencyList(getPricesOnDate, ConsoleColor.Black, ConsoleColor.Gray, line, selectedItem + 1);
                            }
                            else
                            {

                                PrintRowInTheCurrencyList(getPricesOnDate, ConsoleColor.Black, ConsoleColor.Gray, 14, 0);

                                selectedItem = getPricesOnDate.Count;

                                Console.SetCursorPosition((Console.WindowWidth - " Powrot ".Length) / 2, lineBack);
                                Console.BackgroundColor = ConsoleColor.Gray;
                                Console.ForegroundColor = ConsoleColor.Black;
                                Console.Write(" Powrot ");
                                Console.ResetColor();
                            }


                            break;

                        case ConsoleKey.DownArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if (selectedItem < getPricesOnDate.Count - 1)
                            {
                                line = PrintRowInTheCurrencyList(getPricesOnDate, ConsoleColor.Black, ConsoleColor.Gray, line, selectedItem);
                                line = PrintRowInTheCurrencyList(getPricesOnDate, ConsoleColor.Gray, ConsoleColor.Black, line, ++selectedItem);
                            }
                            else if (selectedItem == getPricesOnDate.Count - 1)
                            {
                                line = PrintRowInTheCurrencyList(getPricesOnDate, ConsoleColor.Black, ConsoleColor.Gray, line, selectedItem++);

                                line += 3;
                                Console.SetCursorPosition((Console.WindowWidth - " Powrot ".Length) / 2, lineBack);
                                Console.BackgroundColor = ConsoleColor.Gray;
                                Console.ForegroundColor = ConsoleColor.Black;
                                Console.Write(" Powrot ");
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.SetCursorPosition((Console.WindowWidth - " Powrot ".Length) / 2, lineBack);
                                Console.ResetColor();
                                Console.Write(" Powrot ");
                                Console.ResetColor();

                                selectedItem = 0;
                                line = PrintRowInTheCurrencyList(getPricesOnDate, ConsoleColor.Gray, ConsoleColor.Black, 14, selectedItem);
                            }

                            break;


                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if (selectedItem == getPricesOnDate.Count)
                            {
                                Thread.Sleep(500);
                                Console.ResetColor();
                                return "Powrot";
                            }
                            else
                            {
                                ShowSpecificCurrencyRate(dateCurrency, getPricesOnDate[selectedItem].Name);
                                start = false;
                            }
                            break;
                    }

                }
            }

        }

        private static string ShowSpecificCurrencyRate(DateTime dateCurrency, string currency)
        {
            int selectedColor = 0;
            int selectedItem = 0;

            string[] menuItems = { "Wykres", "Powrot" };

            int width = Console.WindowWidth;
            int height = Console.WindowHeight;

            Dictionary<DateTime, Currency.CurrencyPrice> getPricesUntilDate = null;

            int position_y = 0;

            int line;

            bool start = false;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;
                    width = Console.WindowWidth;
                    height = Console.WindowHeight;

                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    string s = "      ─────────────── KURSY WALUTY " + currency + " do dnia " + dateCurrency.ToString("dd.MM.yyyy") + " ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌───────────────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.Write(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│    Data    │ Kurs (zl) │ Zmiana (%) │ Zmiana (zł) │");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("└───────────────────────────────────────────────────┘");

                    if (getPricesUntilDate == null)
                    {
                        try
                        {
                            Console.SetCursorPosition((Console.WindowWidth - " Trwa ladowanie ".Length) / 2, 16);
                            Console.WriteLine(" Trwa ladowanie ");
                            getPricesUntilDate = Currency.getPricesUntilDateAsync(currency, dateCurrency).Result;
                            Console.Beep(500, 400);
                        }
                        catch (Exception e)
                        {
                            ShowErrorConnectToAPI(0);
                            return "Błąd";
                        }
                    }

                    s = "┌───────────────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write(s);

                    for (int i = 0; i < getPricesUntilDate.Count; i++)
                    {
                        DateTime currentDate = dateCurrency.AddDays(-i);
                        Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                        Console.Write("│            │           │            │             ");
                        Console.ResetColor();
                        Console.Write("│");
                        //▲▼─
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                        Console.Write("│ " + currentDate.ToString("dd.MM.yyyy") + " │");
                        Console.Write("   " + String.Format(CultureInfo.InvariantCulture,
                         "{0:0.000}", getPricesUntilDate[currentDate].Value) + "   │");
                        if (getPricesUntilDate[currentDate].PercentDifference < 0)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write("  " + "▼ " + String.Format(CultureInfo.InvariantCulture,
                         "{0:0.000}", Math.Abs(getPricesUntilDate[currentDate].PercentDifference)) + "%");
                        }
                        else if (getPricesUntilDate[currentDate].PercentDifference > 0)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                            Console.Write("  " + "▲ " + String.Format(CultureInfo.InvariantCulture, "{0:0.000}", getPricesUntilDate[currentDate].PercentDifference) + "%");
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Write("  " + "─ " + String.Format(CultureInfo.InvariantCulture, "{0:0.000}", getPricesUntilDate[currentDate].PercentDifference) + "%");
                        }
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write("  │");
                        if (getPricesUntilDate[currentDate].PriceDifference < 0)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write("   " + "▼ " + String.Format(CultureInfo.InvariantCulture,
                         "{0:0.000}", Math.Abs(getPricesUntilDate[currentDate].PriceDifference)));
                        }
                        else if (getPricesUntilDate[currentDate].PriceDifference > 0)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                            Console.Write("   " + "▲ " + String.Format(CultureInfo.InvariantCulture,
                         "{0:0.000}", getPricesUntilDate[currentDate].PriceDifference));
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Write("   " + "─ " + String.Format(CultureInfo.InvariantCulture,
                         "{0:0.000}", getPricesUntilDate[currentDate].PriceDifference));
                        }
                        Console.Write("   ");
                        Console.ResetColor();
                        Console.Write("│");
                        Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                        Console.Write("│            │           │            │             ");
                        Console.ResetColor();
                        Console.Write("│");
                    }

                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("└───────────────────────────────────────────────────┘");
                    Console.ResetColor();

                    line += 2;
                    position_y = line;


                    ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

                    for (int i = 0; i < menuItems.Length; i++)
                    {
                        if (selectedItem == i)
                        {
                            Console.BackgroundColor = color[0];
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - 10) / 2, line++);
                            Console.Write(" " + menuItems[i] + " ");
                            Console.ResetColor();
                            Console.Write(" ");

                        }
                        else
                        {
                            Console.SetCursorPosition((Console.WindowWidth - 10) / 2, line++);
                            Console.WriteLine(" " + menuItems[i] + " ");

                        }
                    }
                }
                Thread.Sleep(200);



                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.UpArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            int oldSelectedItem;
                            if ((selectedItem - 1) < 0)
                            {
                                oldSelectedItem = selectedItem;
                                selectedItem = menuItems.Length - 1;
                            }
                            else
                            {
                                oldSelectedItem = selectedItem;
                                selectedItem--;
                            }
                            Console.SetCursorPosition((Console.WindowWidth - 10) / 2, position_y + oldSelectedItem);
                            Console.Write(" " + menuItems[oldSelectedItem] + " ");
                            Console.ResetColor();
                            Console.BackgroundColor = ConsoleColor.Gray;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - 10) / 2, position_y + selectedItem);
                            Console.Write(" " + menuItems[selectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            break;

                        case ConsoleKey.DownArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            int oldDownSelectedItem;
                            if ((selectedItem + 1) > menuItems.Length - 1)
                            {
                                oldDownSelectedItem = selectedItem;
                                selectedItem = 0;
                            }
                            else
                            {
                                oldDownSelectedItem = selectedItem;
                                selectedItem++;
                            }
                            Console.SetCursorPosition((Console.WindowWidth - 10) / 2, position_y + oldDownSelectedItem);
                            Console.Write(" " + menuItems[oldDownSelectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            Console.BackgroundColor = ConsoleColor.Gray;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition((Console.WindowWidth - 10) / 2, position_y + selectedItem);
                            Console.Write(" " + menuItems[selectedItem] + " ");
                            Console.ResetColor();
                            Console.Write(" ");
                            break;


                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if (selectedItem == 0)
                            {
                                Console.ResetColor();
                                ShowSpecificCurrencyGraph(dateCurrency, currency);
                                start = false;
                            }
                            else
                            {
                                Thread.Sleep(500);
                                return "P";
                            }
                            break;

                        default:
                            break;
                    }

                }

            }
        }

        private static string ShowSpecificCurrencyGraph(DateTime dateCurrency, string currency)
        {
            string[] menuItems = { "  Powrot  " };

            int width = 0;
            int height = 0;

            int line;

            while (true)
            {
                if (width != Console.WindowWidth || height != Console.WindowHeight)
                {

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;

                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    string s = "───────── WYKRES KURSU WALUTY " + currency + " do dnia " + dateCurrency.ToString("dd.MM.yyyy") + " ─────────";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 3;

                    CreateGraph(ref line, dateCurrency, currency);

                    line += 2;

                    ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };


                    Console.BackgroundColor = color[0];
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.SetCursorPosition((Console.WindowWidth - menuItems[0].Length) / 2, line);
                    Console.Write(" " + menuItems[0] + " ");
                    Console.ResetColor();
                    Console.Write(" ");
                    line++;
                }
                Thread.Sleep(500);



                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            Console.ResetColor();
                            return "Powrot";
                    }

                }

            }
        }

        private static int SelectCurrencyAmount(int selectedItem, int selectedColor)
        {
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── WYKRES NOTOWANIA WALUT ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("           WYBIERZ ILOSC WALUT            ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("└──────────────────────────────────────────┘");

                    ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

                    Console.WriteLine(""); line += 2;
                    position_y = line;
                }

                string[] menuItems = { "1", "2", "3" };

                for (int i = 0; i < menuItems.Length; i++)
                {
                    if (selectedItem == i)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length - 2) / 2, position_y + i);
                        Console.Write(" " + menuItems[i] + " ");
                        Console.ResetColor();
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length - 2) / 2, position_y + i);
                        Console.WriteLine(" " + menuItems[i] + " ");
                    }
                }

                Thread.Sleep(400);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.UpArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if ((selectedItem - 1) < 0)
                            {
                                selectedItem = menuItems.Length - 1;
                            }
                            else
                            {
                                selectedItem--;
                            }

                            break;

                        case ConsoleKey.DownArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if ((selectedItem + 1) > menuItems.Length - 1)
                            {
                                selectedItem = 0;
                            }
                            else
                            {
                                selectedItem++;
                            }
                            break;


                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();

                            return selectedItem + 1;
                    }

                }
            }
        }

        private static int SelectQuotationsAmount(int selectedItem, int selectedColor)
        {
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            bool start = false;
            string s;
            int position_y = 0;

            while (true)
            {
                if (!start || width != Console.WindowWidth || height != Console.WindowHeight)
                {
                    start = true;

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    int line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    s = "      ─────────────── WYKRES NOTOWANIA WALUT ───────────────      ";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 2;

                    s = "┌──────────────────────────────────────────┐";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("        WYBIERZ ILOSC NOTOWAN (DNI)       ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("│");
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("                                          ");
                    Console.ResetColor();
                    Console.Write("│");
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, ++line);
                    Console.Write("└──────────────────────────────────────────┘");

                    ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

                    Console.WriteLine(""); line += 2;
                    position_y = line;
                }

                string[] menuItems = { " 7", "14", "30" };

                for (int i = 0; i < menuItems.Length; i++)
                {
                    if (selectedItem == i)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length - 2) / 2, position_y + i);
                        Console.Write(" " + menuItems[i] + " ");
                        Console.ResetColor();
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.SetCursorPosition((Console.WindowWidth - menuItems[i].Length - 2) / 2, position_y + i);
                        Console.WriteLine(" " + menuItems[i] + " ");
                    }
                }

                Thread.Sleep(400);

                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.UpArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if ((selectedItem - 1) < 0)
                            {
                                selectedItem = menuItems.Length - 1;
                            }
                            else
                            {
                                selectedItem--;
                            }

                            break;

                        case ConsoleKey.DownArrow:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            if ((selectedItem + 1) > menuItems.Length - 1)
                            {
                                selectedItem = 0;
                            }
                            else
                            {
                                selectedItem++;
                            }
                            break;


                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();

                            return Int32.Parse(menuItems[selectedItem]);

                    }

                }
            }
        }

        private static void ChooseGraphOptions()
        {
            int selectedItem = 0;
            int selectedColor = 0;
            int currencyAmount = SelectCurrencyAmount(0, 0);
            string[] currency = new string[3] { null, null, null };

            for (int i = 0; i < currencyAmount; i++)
            {
                int iii = i + 1;
                currency[i] = SelectCurrency(selectedItem, selectedColor, "            WYBIERZ WALUTE NR " + iii + "           ", "      ─────────────── WYKRES NOTOWANIA WALUT ───────────────      ");
            }

            DateTime dateCurrency;
            string stringCurrentDate = DateTime.Now.ToString("dd.MM.yyyy");
            int quantity;

            if (currency[0] == currency[1] || currency[0] == currency[2] || (currency[1] == currency[2] && !String.IsNullOrEmpty(currency[1])))
            {
                ShowErrorTheSameCurrency(selectedColor);
            }
            else
            {
                try
                {
                    stringCurrentDate = SelectCurrencyDateForGraph(stringCurrentDate, "─────────────── WYKRES NOTOWANIA WALUT ───────────────");
                    if (DateTime.TryParseExact(stringCurrentDate, "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                    else if (DateTime.TryParseExact(stringCurrentDate, "d.M.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                    else if (DateTime.TryParseExact(stringCurrentDate, "d.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                    else if (DateTime.TryParseExact(stringCurrentDate, "dd.M.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateCurrency)) { }
                    else { throw new Exception(); }
                    ValidateDate(dateCurrency);
                }
                catch (Exception e)
                {
                    ShowErrorBadDate(selectedColor);
                    return;
                }
                try
                {
                    quantity = SelectQuotationsAmount(0, 0);
                }
                catch (Exception e)
                {
                    ShowErrorBadAmount(selectedColor);
                    return;
                }
                try
                {
                    ValidateDate(dateCurrency, quantity);
                }
                catch(Exception)
                {
                    ShowErrorBadDate(selectedColor);
                    return;
                }
                ShowChoosedCurrencyGraph(dateCurrency, currency, quantity);
            }
            Console.ResetColor();
        }

        private static string ShowChoosedCurrencyGraph(DateTime dateCurrency, string currency)
        {
            string[] menuItems = { "  Powrot  " };

            int width = 0;
            int height = 0;

            int line;

            while (true)
            {
                if (width != Console.WindowWidth || height != Console.WindowHeight)
                {

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;

                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    string s = "───────── WYKRES KURSU WALUTY " + currency + " do dnia " + dateCurrency.ToString("dd.MM.yyyy") + " ─────────";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 3;

                    CreateGraph(ref line, dateCurrency, currency);

                    line += 2;

                    ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };


                    Console.BackgroundColor = color[0];
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.SetCursorPosition((Console.WindowWidth - menuItems[0].Length) / 2, line);
                    Console.Write(" " + menuItems[0] + " ");
                    Console.ResetColor();
                    Console.Write(" ");
                    line++;
                }
                Thread.Sleep(500);



                if (Console.KeyAvailable == true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);

                    switch (cki.Key)
                    {
                        case ConsoleKey.Enter:
                            new Thread(() => Console.Beep(500, 200)).Start();
                            Thread.Sleep(500);
                            return "P";
                    }

                }

            }
        }

        private static string ShowChoosedCurrencyGraph(DateTime dateCurrency, string[] currency, int quantity)
        {
            string[] menuItems = { "  Powrot  " };

            int width = 0;
            int height = 0;

            bool result = true;

            int line;

            while (true)
            {
                if (width != Console.WindowWidth || height != Console.WindowHeight)
                {

                    width = Console.WindowWidth;
                    height = Console.WindowHeight;

                    Console.ResetColor();
                    Console.Clear();
                    Console.CursorVisible = false;
                    line = 2;

                    InitializeProgramLogo(line);
                    line = 9;

                    Console.ForegroundColor = ConsoleColor.Blue;
                    string s = "───────── WYKRES KURSU WALUT DO DNIA " + dateCurrency.ToString("dd.MM.yyyy") + " ─────────";
                    Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, line);
                    Console.WriteLine(s);
                    Console.ResetColor();

                    line += 3;

                    result = CreateGraphForAFewCurrency(dateCurrency, currency, quantity);

                    line = Console.CursorTop + 3;

                    ConsoleColor[] color = { ConsoleColor.Gray, ConsoleColor.Cyan, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Yellow };

                    if (result)
                    {
                        Console.BackgroundColor = color[0];
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.SetCursorPosition((Console.WindowWidth - menuItems[0].Length) / 2, line);
                        Console.Write(" " + menuItems[0] + " ");
                        Console.ResetColor();
                        Console.Write(" ");
                        line++;
                    }
                }
                Thread.Sleep(500);

                if (result)
                {
                    if (Console.KeyAvailable == true)
                    {
                        ConsoleKeyInfo cki = Console.ReadKey(true);

                        switch (cki.Key)
                        {
                            case ConsoleKey.Enter:
                                new Thread(() => Console.Beep(500, 200)).Start();
                                return "P";
                        }

                    }
                }
                else
                    return "P";

            }
        }

        static void fillUp(char[] line, char WithChar = '\0')
        {
            for (int i = 0; i < line.Length; i++)
            {
                line[i] = WithChar;
            }
        }
        static void fillUp2(char[] line, char WithChar = '\0')
        {
            int c = 0;
            for (int i = 0; i < line.Length; i++)
            {
                if (i < 6)
                    line[i] = ' ';
                else if (i == 6)
                    line[i] = '└';
                else
                {
                    if (c++ % 3 == 2)
                        line[i] = WithChar;
                    else
                        line[i] = '─';
                }

            }
        }

        static void fillUp3(char[] line, char WithChar = '\0')
        {
            int c = 0;
            for (int i = 0; i < line.Length; i++)
            {
                if (i < 6)
                    line[i] = ' ';
                else if (i == 6)
                    line[i] = '└';
                else
                {
                    if (c++ % 3 == 2)
                        line[i] = WithChar;
                    else
                        line[i] = '─';
                }

            }
        }

        static void CreateGraph(ref int l, DateTime dateCurrency, string currency3)
        {
            char[] LINE = new char[8 * 3 + 4 + 5];
            fillUp(LINE, WithChar: BLANK);
            int position_y = Console.CursorTop;
            Dictionary<DateTime, Currency.CurrencyPrice> getPricesUntilDate;
            try
            {
                /*Console.SetCursorPosition((Console.WindowWidth - " Trwa ladowanie ".Length) / 2, 16);
                Console.WriteLine(" Trwa ladowanie ");*/
                getPricesUntilDate = Currency.getPricesUntilDateToGraphAsync(currency3, dateCurrency).Result;
                //Console.Beep(500, 400);
            }
            catch (Exception e)
            {
                ShowErrorConnectToAPI(0);
                return;
            }

            double min = getPricesUntilDate[dateCurrency].Value, max = getPricesUntilDate[dateCurrency].Value;

            for (int ii = 1; ii < getPricesUntilDate.Count; ii++)
            {
                if (min > getPricesUntilDate[dateCurrency.AddDays(-ii)].Value)
                    min = getPricesUntilDate[dateCurrency.AddDays(-ii)].Value;
                if (max < getPricesUntilDate[dateCurrency.AddDays(-ii)].Value)
                    max = getPricesUntilDate[dateCurrency.AddDays(-ii)].Value;
            }

            if (min == max)
            {
                ShowErrorCreateGraph(0);
                return;
            }

            double maxval = max; //arbitrary values
            double delta = (max - min) / (getPricesUntilDate.Count - 1); //size of iteration steps
            int loc;
            LINE[0] = '┤';
            int j = 0;
            int i = 0;


            List<double[]> currency = new List<double[]>();
            List<double[]> currency2 = new List<double[]>();
            for (double x = min; x < maxval; x += delta) //0.0001 to avoid DIV/0 error
            {
                double[] curr = new double[2];
                curr[0] = i++;
                curr[1] = x;
                currency.Add(curr);
                //loc = (int)Math.Round(i * cHalf) + cHalf;
                // LINE[loc] = X;
                //Console.ForegroundColor = ConsoleColor.Blue;
                /*Console.WriteLine(LINE);
                Console.ResetColor();
                fillUp(LINE, WithChar: BLANK); // blank the line, remove X point
                LINE[0] = '┤'; // for horizontal axis*/
            }
            for (int x = 0; x < getPricesUntilDate.Count; x++) //0.0001 to avoid DIV/0 error
            {
                double[] curr2 = new double[2];
                double delta2 = Math.Abs(currency[0][1] - getPricesUntilDate[dateCurrency.AddDays(-7 + x)].Value);
                double delta3 = 0;
                for (j = 1; j < currency.Count; j++)
                {
                    if (delta2 > Math.Abs(currency[j][1] - getPricesUntilDate[dateCurrency.AddDays(-7 + x)].Value))
                    {
                        delta2 = Math.Abs(currency[j][1] - getPricesUntilDate[dateCurrency.AddDays(-7 + x)].Value);
                        delta3 = (double)j;
                    }
                }
                curr2[0] = x;
                curr2[1] = delta3;
                currency2.Add(curr2);
                //loc = (int)Math.Round(i * cHalf) + cHalf;
                // LINE[loc] = X;
                //Console.ForegroundColor = ConsoleColor.Blue;
                /*Console.WriteLine(LINE);
                Console.ResetColor();
                fillUp(LINE, WithChar: BLANK); // blank the line, remove X point
                LINE[0] = '┤'; // for horizontal axis*/
            }
            Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, l++);
            Console.Write("Kurs (zl)"); // line of dots for "vertical" axis
            l++;
            for (int x = currency.Count - 1; x >= 0; x--) //0.0001 to avoid DIV/0 error
            {
                int ix = 0;
                for (i = 0; i < currency2.Count; i++)
                {
                    ix += 3;
                    if (currency2[i][1] == x)
                        LINE[ix] = '■';
                }
                //loc = (int)Math.Round(i * cHalf) + cHalf;
                // LINE[loc] = X;
                //Console.ForegroundColor = ConsoleColor.Blue;
                Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, l++);
                Console.Write(String.Format(CultureInfo.InvariantCulture,
                         "{0:0.000}", currency[x][1]) + " ");
                Console.Write(LINE[0]);
                Console.ForegroundColor = ConsoleColor.Red;
                for (int c = 1; c < LINE.Length; c++)
                    Console.Write(LINE[c]);
                Console.ResetColor();
                Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, l++);
                Console.Write("      │");
                fillUp(LINE, WithChar: BLANK); // blank the line, remove X point                
                LINE[0] = '┤'; // for horizontal axis
            }
            Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, l++);
            fillUp3(LINE, WithChar: DOT);
            Console.Write(LINE); // line of dots for "vertical" axis
            l++;
            Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, l++);
            Console.Write("Dzien   "); // line of dots for "vertical" axis
            for (int c = 0; c < 8; c++)
                Console.Write(dateCurrency.AddDays(-7 + c).ToString("dd") + " "); // line of dots for "vertical" axis
            l++;
            Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, l++);
            Console.Write("Miesiac "); // line of dots for "vertical" axis
            for (int c = 0; c < 8; c++)
                Console.Write(dateCurrency.AddDays(-7 + c).ToString("MM") + " "); // line of dots for "vertical" axis
            l++;
            Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, l++);
            Console.Write("Rok     "); // line of dots for "vertical" axis
            for (int c = 0; c < 8; c++)
                Console.Write(dateCurrency.AddDays(-7 + c).ToString("yy") + " "); // line of dots for "vertical" axis
        }

        static bool CreateGraphForAFewCurrency(DateTime dateCurrency, string[] currency3, int quantity)
        {

            Console.WriteLine();
            Console.WriteLine();

            char[] LINE = new char[quantity * 3 + 4 + 7];

            try
            {
                Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, Console.CursorTop);
            }
            catch (Exception e)
            {
                ShowErrorResolutionScreen(0);
                return false;
            }
            for (int g = 0; g < currency3.Length; g++)
            {
                if (!String.IsNullOrEmpty(currency3[g]))
                {
                    Console.Write("      ");
                    if (g == 0)
                        Console.BackgroundColor = ConsoleColor.Red;
                    else if (g == 1)
                        Console.BackgroundColor = ConsoleColor.Blue;
                    else
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.Write("  ");
                    Console.ResetColor();
                    Console.Write(" - " + currency3[g]);
                }
            }

            fillUp(LINE, WithChar: BLANK);
            int position_y = Console.CursorTop;

            Dictionary<DateTime, Currency.CurrencyPrice> firstPricesUntilDate = null;
            Dictionary<DateTime, Currency.CurrencyPrice> secondPricesUntilDate = null;
            Dictionary<DateTime, Currency.CurrencyPrice> thirdPricesUntilDate = null;

            var t1 = Task.Run(() =>
            {
                for (int q = 0; q < currency3.Length; q++)
                {
                    switch (q)
                    {
                        case 0:
                            try
                            {
                                if (currency3[0] != null)
                                    firstPricesUntilDate = Currency.getPricesUntilDateToGraph(currency3[0], dateCurrency, quantity);
                            }
                            catch (Exception e)
                            {
                                ///ShowErrorConnectToAPI(0);
                                return;
                            }

                            Thread.Sleep(500);
                            break;

                        case 1:
                            try
                            {
                                if (currency3[1] != null)
                                    secondPricesUntilDate = Currency.getPricesUntilDateToGraph(currency3[1], dateCurrency, quantity);
                            }
                            catch (Exception e)
                            {
                                //throw new Exception();
                                //ShowErrorConnectToAPI(0);
                                return;
                            }

                            Thread.Sleep(1000);
                            break;

                        case 2:
                            try
                            {
                                if (currency3[2] != null)
                                    thirdPricesUntilDate = Currency.getPricesUntilDateToGraph(currency3[2], dateCurrency, quantity);
                            }
                            catch (Exception e)
                            {
                                //ShowErrorConnectToAPI(0);
                                return;
                            }

                            //Thread.Sleep(500);
                            break;
                    }
                }
            });

            var t2 = Task.Run(() =>
            {
                using (var progress = new ProgressBar())
                {
                    Console.SetCursorPosition((Console.WindowWidth - 18) / 2, 16);
                    for (int z = 0; z <= 100; z++)
                    {
                        progress.Report((double)z / 100);
                        Thread.Sleep(currency3.Length * quantity);
                    }
                }
            });

            Task.WaitAll(new[] { t1, t2 }, 20000);

            if (firstPricesUntilDate == null || (currency3[1] != null && secondPricesUntilDate == null) || (currency3[2] != null && thirdPricesUntilDate == null))
            {
                ShowErrorConnectToAPI(0);
                return false;
            }


            double min = firstPricesUntilDate[dateCurrency].Value, max = firstPricesUntilDate[dateCurrency].Value;

            for (int ii = 0; ii < firstPricesUntilDate.Count; ii++)
            {
                if (min > firstPricesUntilDate[dateCurrency.AddDays(-ii)].Value)
                    min = firstPricesUntilDate[dateCurrency.AddDays(-ii)].Value;
                if (max < firstPricesUntilDate[dateCurrency.AddDays(-ii)].Value)
                    max = firstPricesUntilDate[dateCurrency.AddDays(-ii)].Value;
                if (secondPricesUntilDate != null)
                {
                    if (max < secondPricesUntilDate[dateCurrency.AddDays(-ii)].Value)
                        max = secondPricesUntilDate[dateCurrency.AddDays(-ii)].Value;
                    if (min > secondPricesUntilDate[dateCurrency.AddDays(-ii)].Value)
                        min = secondPricesUntilDate[dateCurrency.AddDays(-ii)].Value;
                }
                if (thirdPricesUntilDate != null)
                {
                    if (min > thirdPricesUntilDate[dateCurrency.AddDays(-ii)].Value)
                        min = thirdPricesUntilDate[dateCurrency.AddDays(-ii)].Value;
                    if (max < thirdPricesUntilDate[dateCurrency.AddDays(-ii)].Value)
                        max = thirdPricesUntilDate[dateCurrency.AddDays(-ii)].Value;
                }
            }

            if (min == max)
            {
                ShowErrorCreateGraph(0);
                return false;
            }

            double maxval = max; //arbitrary values
            double delta = (max - min) / (firstPricesUntilDate.Count - 1); //size of iteration steps
            int loc;
            LINE[0] = '┤';
            int j = 0;
            int i = 0;


            List<double[]> currency = new List<double[]>();
            List<double[]> currency2 = new List<double[]>();
            for (double x = min; x < maxval; x += delta) //0.0001 to avoid DIV/0 error
            {
                double[] curr = new double[2];
                curr[0] = i++;
                curr[1] = x;
                currency.Add(curr);
                //loc = (int)Math.Round(i * cHalf) + cHalf;
                // LINE[loc] = X;
                //Console.ForegroundColor = ConsoleColor.Blue;
                /*Console.WriteLine(LINE);
                Console.ResetColor();
                fillUp(LINE, WithChar: BLANK); // blank the line, remove X point
                LINE[0] = '┤'; // for horizontal axis*/
            }

            for (int x = 0; x < firstPricesUntilDate.Count; x++) //0.0001 to avoid DIV/0 error
            {
                double[] curr2 = new double[2];
                double delta2 = Math.Abs(currency[0][1] - firstPricesUntilDate[dateCurrency.AddDays(-quantity + x)].Value);
                double delta3 = 0;
                for (j = 1; j < currency.Count; j++)
                {
                    if (delta2 > Math.Abs(currency[j][1] - firstPricesUntilDate[dateCurrency.AddDays(-quantity + x)].Value))
                    {
                        delta2 = Math.Abs(currency[j][1] - firstPricesUntilDate[dateCurrency.AddDays(-quantity + x)].Value);
                        delta3 = (double)j;
                    }
                }
                curr2[0] = x;
                curr2[1] = delta3;
                currency2.Add(curr2);
                //loc = (int)Math.Round(i * cHalf) + cHalf;
                // LINE[loc] = X;
                //Console.ForegroundColor = ConsoleColor.Blue;
                /*Console.WriteLine(LINE);
                Console.ResetColor();
                fillUp(LINE, WithChar: BLANK); // blank the line, remove X point
                LINE[0] = '┤'; // for horizontal axis*/
            }
            try
            {
                Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, Console.CursorTop - 1);
            }
            catch (Exception e)
            {
                ShowErrorResolutionScreen(0);
                return false;
            }
            Console.WriteLine("Kurs (zl)"); // line of dots for "vertical" axis
            Console.WriteLine();
            fillUp(LINE, WithChar: BLANK);
            LINE[0] = '┤';
            position_y = Console.CursorTop;
            int y = Console.CursorTop;
            for (int x = currency.Count - 1; x >= 0; x--) //0.0001 to avoid DIV/0 error
            {
                int ix = 0;
                for (i = 0; i < currency2.Count; i++)
                {
                    ix += 3;
                    if (currency2[i][1] == x)
                        LINE[ix] = '■';
                }
                //loc = (int)Math.Round(i * cHalf) + cHalf;
                // LINE[loc] = X;
                //Console.ForegroundColor = ConsoleColor.Blue;
                Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
                Console.Write("" + String.Format(CultureInfo.InvariantCulture,
                         "{0:0.000}", currency[x][1]) + " ");
                Console.Write(LINE[0]);
                Console.ForegroundColor = ConsoleColor.Red;
                for (int c = 1; c < LINE.Length; c++)
                    Console.Write(LINE[c]);
                Console.ResetColor();
                Console.WriteLine();
                Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
                Console.WriteLine("      │");
                fillUp(LINE, WithChar: BLANK); // blank the line, remove X point                
                LINE[0] = '┤'; // for horizontal axis
            }
            if (secondPricesUntilDate != null)
            {
                fillUp(LINE, WithChar: BLANK);
                LINE[0] = '┤';
                currency2 = new List<double[]>();
                Console.SetCursorPosition(0, position_y);
                y = position_y;
                for (int x = 0; x < secondPricesUntilDate.Count; x++) //0.0001 to avoid DIV/0 error
                {
                    double[] curr2 = new double[2];
                    double delta2 = Math.Abs(currency[0][1] - secondPricesUntilDate[dateCurrency.AddDays(-quantity + x)].Value);
                    double delta3 = 0;
                    for (j = 1; j < currency.Count; j++)
                    {
                        if (delta2 > Math.Abs(currency[j][1] - secondPricesUntilDate[dateCurrency.AddDays(-quantity + x)].Value))
                        {
                            delta2 = Math.Abs(currency[j][1] - secondPricesUntilDate[dateCurrency.AddDays(-quantity + x)].Value);
                            delta3 = (double)j;
                        }
                    }
                    curr2[0] = x;
                    curr2[1] = delta3;
                    currency2.Add(curr2);
                    //loc = (int)Math.Round(i * cHalf) + cHalf;
                    // LINE[loc] = X;
                    //Console.ForegroundColor = ConsoleColor.Blue;
                    /*Console.WriteLine(LINE);
                    Console.ResetColor();
                    fillUp(LINE, WithChar: BLANK); // blank the line, remove X point
                    LINE[0] = '┤'; // for horizontal axis*/
                }
                for (int x = currency.Count - 1; x >= 0; x--) //0.0001 to avoid DIV/0 error
                {
                    int ix = 0;
                    for (i = 0; i < currency2.Count; i++)
                    {
                        ix += 3;
                        if (currency2[i][1] == x)
                            LINE[ix] = '■';
                    }
                    //loc = (int)Math.Round(i * cHalf) + cHalf;
                    // LINE[loc] = X;
                    //Console.ForegroundColor = ConsoleColor.Blue;
                    Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
                    Console.Write("" + String.Format(CultureInfo.InvariantCulture,
                             "{0:0.000}", currency[x][1]) + " ");
                    Console.Write(LINE[0]);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    for (int c = 1; c < LINE.Length - 2; c++)
                    {
                        if (!Char.IsWhiteSpace(LINE[c]))
                            Console.Write(LINE[c]);
                        else
                        {
                            Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
                        }
                    }

                    Console.ResetColor();
                    Console.WriteLine();
                    Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
                    Console.WriteLine("      │");
                    fillUp(LINE, WithChar: BLANK); // blank the line, remove X point                
                    LINE[0] = '┤'; // for horizontal axis
                }
            }

            if (thirdPricesUntilDate != null)
            {
                currency2 = new List<double[]>();
                Console.SetCursorPosition(0, position_y);
                y = position_y;
                for (int x = 0; x < thirdPricesUntilDate.Count; x++) //0.0001 to avoid DIV/0 error
                {
                    double[] curr2 = new double[2];
                    double delta2 = Math.Abs(currency[0][1] - thirdPricesUntilDate[dateCurrency.AddDays(-quantity + x)].Value);
                    double delta3 = 0;
                    for (j = 1; j < currency.Count; j++)
                    {
                        if (delta2 > Math.Abs(currency[j][1] - thirdPricesUntilDate[dateCurrency.AddDays(-quantity + x)].Value))
                        {
                            delta2 = Math.Abs(currency[j][1] - thirdPricesUntilDate[dateCurrency.AddDays(-quantity + x)].Value);
                            delta3 = (double)j;
                        }
                    }
                    curr2[0] = x;
                    curr2[1] = delta3;
                    currency2.Add(curr2);
                    //loc = (int)Math.Round(i * cHalf) + cHalf;
                    // LINE[loc] = X;
                    //Console.ForegroundColor = ConsoleColor.Blue;
                    /*Console.WriteLine(LINE);
                    Console.ResetColor();
                    fillUp(LINE, WithChar: BLANK); // blank the line, remove X point
                    LINE[0] = '┤'; // for horizontal axis*/
                }
                for (int x = currency.Count - 1; x >= 0; x--) //0.0001 to avoid DIV/0 error
                {
                    int ix = 0;
                    for (i = 0; i < currency2.Count; i++)
                    {
                        ix += 3;
                        if (currency2[i][1] == x)
                            LINE[ix] = '■';
                    }
                    //loc = (int)Math.Round(i * cHalf) + cHalf;
                    // LINE[loc] = X;
                    //Console.ForegroundColor = ConsoleColor.Blue;
                    Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
                    Console.Write("" + String.Format(CultureInfo.InvariantCulture,
                             "{0:0.000}", currency[x][1]) + " ");
                    Console.Write(LINE[0]);
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    for (int c = 1; c < LINE.Length - 2; c++)
                    {
                        if (!Char.IsWhiteSpace(LINE[c]))
                            Console.Write(LINE[c]);
                        else
                            Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
                    }

                    Console.ResetColor();
                    Console.WriteLine();
                    Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
                    Console.WriteLine("      │");
                    fillUp(LINE, WithChar: BLANK); // blank the line, remove X point                
                    LINE[0] = '┤'; // for horizontal axis
                }
            }
            Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
            y++;
            fillUp2(LINE, WithChar: DOT);
            Console.WriteLine(LINE); // line of dots for "vertical" axis
            Console.WriteLine();
            Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
            y++;
            Console.Write("Dzien   "); // line of dots for "vertical" axis
            for (int c = 0; c < quantity + 1; c++)
                Console.Write(dateCurrency.AddDays(-quantity + c).ToString("dd") + " "); // line of dots for "vertical" axis
            Console.WriteLine();
            Console.WriteLine();
            Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
            y++;
            Console.Write("Miesiac "); // line of dots for "vertical" axis
            for (int c = 0; c < quantity + 1; c++)
                Console.Write(dateCurrency.AddDays(-quantity + c).ToString("MM") + " "); // line of dots for "vertical" axis
            Console.WriteLine();
            Console.WriteLine();
            Console.SetCursorPosition((Console.WindowWidth - LINE.Length) / 2, y++);
            y++;
            Console.Write("Rok     "); // line of dots for "vertical" axis
            for (int c = 0; c < quantity + 1; c++)
                Console.Write(dateCurrency.AddDays(-quantity + c).ToString("yy") + " "); // line of dots for "vertical" axis


            return true;

        }

    }
}


