﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixerSharp;
using System.Threading;
using System.Collections.ObjectModel;

namespace Graficznie
{
    public class CurrencyPrice
    {
        public string Name { get; set; }
        public double Value { get; set; }
        public double PercentDifference { get; set; }
        public double PriceDifference { get; set; }
        public DateTime Date { get; set; }
        public string DateText
        {
            get
            {
                return Date.ToString("d.MM.yy").Replace(".", "\n");
            }
        }

        public CurrencyPrice(string name, double value, DateTime date, double percentDifference = 0, double priceDifference = 0)
        {
            this.Name = name;
            this.Value = value;
            this.PercentDifference = percentDifference;
            this.PriceDifference = priceDifference;
            this.Date = date;
        }

    }
    static class Currency
    {
        public static int count = 0;

        public static IStrategy Strategy { get; set; }

        public static string[] symbols = new string[]
        {
            "AUD",
            "BGN",
            "BRL",
            "CAD",
            "CHF",
            "CNY",
            "CZK",
            "DKK",
            "EUR",
            "GBP",
            "HKD",
            "HRK",
            "HUF",
            //"IDR",
            "ILS",
            "INR",
            "JPY",
            "KRW",
            "MXN",
            "MYR",
            "NOK",
            "NZD",
            "PHP",
            //"PLN",
            "RON",
            "RUB",
            "SEK",
            "SGD",
            "THB",
            "TRY",
            "USD",
            "ZAR"
        };

        public static Dictionary<string, string> fullNamesDictionary = new Dictionary<string, string>
        {
            { "AUD","Dolar australijski" },
            { "BGN","Lew bułgarski" },
            { "BRL","Real brazylijski" },
            { "CAD","Dolar kanadyjski" },
            { "CHF","Frank szwajcarski" },
            { "CNY","Renminbi" },
            { "CZK","Korona czeska" },
            { "DKK","Korona duńska" },
            { "EUR","Euro" },
            { "GBP","Funt szterling" },
            { "HKD","Dolar hongkoński" },
            { "HRK","Kuna chorwacka" },
            { "HUF","Forint węgierski" },
            { "IDR","Rupia indonezyjska" },
            { "ILS","Nowy izraelski szekel" },
            { "INR","Rupa indyjska" },
            { "JPY","Jen japoński" },
            { "KRW","Won południowokoreański" },
            { "MXN","Peso meksykańskie" },
            { "MYR","Ringgit" },
            { "NOK","Korona norweska" },
            { "NZD","Dolar nowozelandzki" },
            { "PHP","Peso filipińskie" },
            { "PLN","Złoty" },
            { "RON","Lej rumuński" },
            { "RUB","Rubel rosyjski" },
            { "SEK","Korona szwedzka" },
            { "SGD","Dolar singapurski" },
            { "THB","Bat tajlandzki"},
            { "TRY","Lira turecka"},
            { "USD","Dolar amerykański"},
            { "ZAR","Rand południowoafrykański"},
        };

        public static string[] symbolsForCalculator = new string[]
        {
            "AUD",
            "BGN",
            "BRL",
            "CAD",
            "CHF",
            "CNY",
            "CZK",
            "DKK",
            "EUR",
            "GBP",
            "HKD",
            "HRK",
            "HUF",
            //"IDR",
            "ILS",
            "INR",
            "JPY",
            "KRW",
            "MXN",
            "MYR",
            "NOK",
            "NZD",
            "PHP",
            "PLN",
            "RON",
            "RUB",
            "SEK",
            "SGD",
            "THB",
            "TRY",
            "USD",
            "ZAR"
        };

        static PriceInterface currencyPrice = new ProxyPrice();

        public static List<CurrencyPrice> getPricesOnDate(DateTime date)
        {
            List<CurrencyPrice> list = new List<CurrencyPrice>();

            IIterator iterator = new CurrencySymbolsIterator(symbolsForCalculator);

            while (iterator.HasNext())
            {
                try
                {
                    string c = iterator.Next() as string;
                    double price = currencyPrice.Convert(c, "PLN", 1, date);
                    if (!Double.IsInfinity(price) && !Double.IsNaN(price))
                    {
                        double price7DaysEarlier = currencyPrice.Convert(c, "PLN", 1, date.AddDays(-7));
                        double percentDifference = (price - price7DaysEarlier) / price7DaysEarlier * 100;

                        //percentDifference = Math.Round(percentDifference, 3);

                        list.Add(new CurrencyPrice(c, price, date, percentDifference, price - price7DaysEarlier));
                    }
                }
                catch
                {
                    return null;
                }
            }
            return list;
        }

        public async static Task<List<CurrencyPrice>> getPricesOnDateAsync(DateTime date)
        {
            return await Task.Run(() => getPricesOnDate(date));
        }

        public static Task<List<CurrencyPrice>> getPricesStrategyAsync(string currency, DateTime dateTo, int numberOfDaysBack = 7)
        {
            return Task.Run(() => Strategy.createData(currency, dateTo, numberOfDaysBack));
        }
    }
}
