﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graficznie
{
    public interface IIterator
    {
        object Next();

        bool HasNext();
    }

    public class CurrencySymbolsToCalculatorIterator : IIterator
    {
        string[] symbols;
        int position = 0;
        public CurrencySymbolsToCalculatorIterator(string[] symbols)
        {
            this.symbols = symbols;
        }

        public bool HasNext()
        {
            return symbols.Length > position;
        }

        object IIterator.Next()
        {
            if (this.HasNext())
                return symbols[position++];
            else
                return null;
        }
    }

    public class CurrencySymbolsIterator : IIterator
    {
        string[] symbols;
        int position = 0;
        public CurrencySymbolsIterator(string[] symbols)
        {
            this.symbols = symbols;
        }

        public bool HasNext()
        {
            if (symbols.Length > position && symbols[position] == "PLN")
                position++;
            return symbols.Length > position;
        }

        object IIterator.Next()
        {
            if (this.HasNext())
                return symbols[position++];
            else
                return null;
        }
    }
}
