﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Graficznie.ViewModel;

namespace Graficznie
{
    /// <summary>
    /// Interaction logic for CurrencyCalculator.xaml
    /// </summary>
    public partial class CurrencyCalculator : Window
    {
        public CurrencyCalculator()
        {
            this.DataContext = CurrencyCalculatorViewModel.Instance;
            InitializeComponent();
        }
    }
}
