﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Graficznie
{
    class Factory
    {
        public static Window CreateWindow(object arg)
        {
            if (arg is CurrencyPrice) return new SpecificCurrency(arg);
            if (arg is List<CurrencyPrice>) return new GenerateGraph(arg);
            if (arg.Equals("GenerateGraphDiffrentProperties")) return new GenerateGraphDiffrentProperties();
            if (arg.Equals("CurrencyExchange")) return new CurrencyExchange();
            if (arg.Equals("CurrencyCalculator")) return new CurrencyCalculator();
            return null;
        }

    }
}
