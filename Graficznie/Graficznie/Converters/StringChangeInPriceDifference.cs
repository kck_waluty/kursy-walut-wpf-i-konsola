﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Graficznie
{
    public class StringChangeInPriceDifference : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if ((double)value < 0)
                return "▼ " + String.Format(CultureInfo.InvariantCulture,
                "{0:0.0000}", Math.Abs((double)value)) + " zł";
            else if ((double)value > 0)
                return "▲ " + String.Format(CultureInfo.InvariantCulture,
                "{0:0.0000}", (double)value) + " zł";
            else
                return "─ " + String.Format(CultureInfo.InvariantCulture,
                "{0:0.0000}", (double)value) + " zł";
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
