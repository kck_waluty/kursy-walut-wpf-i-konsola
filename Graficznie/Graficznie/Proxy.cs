﻿using FixerSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Graficznie
{
    interface PriceInterface
    {
        double Convert(string from, string to, double amount, DateTime date);
    }

    class Price : PriceInterface
    {
        public double Convert(string from, string to, double amount, DateTime date)
        {
            return Fixer.Convert(from, to, amount, date);
        }
    }

    class ProxyPrice : PriceInterface
    {

        private Price price = new Price();
        bool error = false;
        public double Convert(string from, string to, double amount, DateTime date)
        {
            try
            {
                return price.Convert(from, to, amount, date);
            }
            catch
            {
                Thread.Sleep(1000);
                return price.Convert(from, to, amount, date);
            }
        }
    }
}
