﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Prism.Commands;
using Prism.Mvvm;

namespace Graficznie.ViewModel
{
    class MainWindowViewModel : BindableBase, INotifyPropertyChanged
    {
        public CurrencyPrice[] currentCurrencies = new CurrencyPrice[4];

        public CurrencyPrice[] CurrentCurrencies
        {
            get
            {
                return currentCurrencies;
            }
        }

        public DateTime CurrencyDate
        {
            get
            {
                return DateTime.Now;
            }
        }

        public string CurrencyDateText
        {
            get
            {
                return "Data waluty: " + CurrencyDate.ToString("d.MM.yyyy");
            }
        }

        List<CurrencyPrice> list;
        Random rnd = new Random();
        DispatcherTimer dispatcherTimer;

        double loadCurrencyExchangeProgressBar = 0;

        public double LoadCurrencyExchangeProgressBar
        {
            get
            {
                return loadCurrencyExchangeProgressBar;
            }
            set
            {
                loadCurrencyExchangeProgressBar = value;
                OnPropertyChanged(null);
            }
        }

        Visibility errorLoadCurrencyExchangePanelVisibility = Visibility.Collapsed;

        public Visibility ErrorLoadCurrencyExchangePanelVisibility
        {
            get
            {
                return errorLoadCurrencyExchangePanelVisibility;
            }
            set
            {
                errorLoadCurrencyExchangePanelVisibility = value;
                OnPropertyChanged(null);
            }

        }

        Visibility loadCurrencyExchangePanelVisibility = Visibility.Visible;

        public Visibility LoadCurrencyExchangePanelVisibility
        {
            get
            {
                return loadCurrencyExchangePanelVisibility;
            }
            set
            {
                loadCurrencyExchangePanelVisibility = value;
                OnPropertyChanged(null);
            }

        }

        Visibility currencyInfoVisibility = Visibility.Collapsed;

        public Visibility CurrencyInfoVisibility
        {
            get
            {
                return currencyInfoVisibility;
            }
            set
            {
                currencyInfoVisibility = value;
                OnPropertyChanged(null);
            }
        }

        private void dispatcher()
        {
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 8000);
            dispatcherTimer.Start();

            try
            {
                currentCurrencies[0] = list[4];
                currentCurrencies[1] = list[8];
                currentCurrencies[2] = list[9];
                currentCurrencies[3] = list[28];
                CurrencyInfoVisibility = Visibility.Visible;
            }
            catch { }
            
            

        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (list != null)
            {
                int[] currencies = new int[4];

                for (int i = 0; i < currencies.Length; i++)
                {
                    currencies[i] = rnd.Next(0, list.Count - 1);
                }

                while (currencies[0] == currencies[1])
                {
                    currencies[1] = rnd.Next(0, list.Count - 1);
                }
                while (currencies[0] == currencies[2] || currencies[1] == currencies[2])
                {
                    currencies[2] = rnd.Next(0, list.Count - 1);
                }
                while (currencies[0] == currencies[3] || currencies[1] == currencies[3] || currencies[2] == currencies[3])
                {
                    currencies[3] = rnd.Next(0, list.Count - 1);
                }

                CurrencyInfoVisibility = Visibility.Visible;

                for (int i = 0; i < currentCurrencies.Length; i++)
                {
                    currentCurrencies[i] = list[currencies[i]];
                }

                OnPropertyChanged(null);

            }
            else
            {
                LoadData();
            }
        }

        async Task Progress()
        {
            var progress = new Progress<int>(value => LoadCurrencyExchangeProgressBar = value);
            await Task.Run(() =>
            {
                for (int i = 0; i < 100; i++)
                {
                    ((IProgress<int>)progress).Report(i);
                    Thread.Sleep(30);
                }
            });
        }

        async Task getData()
        {
            list = null;
            //You need find an async API for file copy, and System.IO has a lot to offer.
            //Also, there is no reason to create a Task for MyAsyncFileCopyMethod - the UI
            // will not wait (blocked) for the operation to complete if you use await:   
            list = await Currency.getPricesOnDateAsync(DateTime.Now);
            if (list == null)
                throw new Exception();
        }

        public async void LoadData()
        {
            try
            {
                LoadCurrencyExchangePanelVisibility = Visibility.Visible;
                ErrorLoadCurrencyExchangePanelVisibility = Visibility.Collapsed;
                await getData();
                await Progress();
                LoadCurrencyExchangePanelVisibility = Visibility.Collapsed;
            }
            catch
            {
                LoadCurrencyExchangePanelVisibility = Visibility.Collapsed;
                ErrorLoadCurrencyExchangePanelVisibility = Visibility.Visible;
            }

            if (dispatcherTimer == null)
                dispatcher();

            loadCurrencyExchangeProgressBar = 0;
        }

        public ICommand ShowWindowCommand
        {
            get;
            internal set;
        }       

        public MainWindowViewModel()
        {
            LoadData();
            ShowWindowCommand = new DelegateCommand<object>(ShowWindowExecute);
            SupportingCommand = new DelegateCommand<object>(SupportingExecute);
        }

        private void ShowWindowExecute(object arg)
        {
            Factory.CreateWindow(arg).Show();
        }

        public ICommand SupportingCommand
        {
            get;
            internal set;
        }

        private void SupportingExecute(object arg)
        {
            if (arg.Equals("Exit"))
                Environment.Exit(0);
            else if (arg.Equals("Help"))
                MessageBox.Show("Główny widok aplikacji zawiera menu umożliwiające skorzystanie z opisanych wcześniej funkcjonalności. Wybór żądanej pozycji odbywa się przy pomocy myszy lub klawiatury (klawisze strzałek oraz Enter).\n\n1. Zestawienie kursu walut – po wybraniu tej opcji użytkownik powinien określić datę notowań. W dalszej kolejności wyświetlone zostanie wspomniane zestawienie kursów wszystkich dostępnych w systemie walut, w formie tabeli. Dwukrotne kliknięcie wybranej pozycji spowoduje wyświetlenie nowego widoku, zawierającego zestawienie kursów danej waluty z ostatnich 7 dni, wraz z opcją wygenerowania wykresu." +
                            "\n2. Kalkulator walut – w tym widoku użytkownik wybiera walutę źródłową i docelową konwersji, następnie określa datę notowań oraz kwotę, która ma zostać przeliczona. W rezultacie wyświetlana jest przeliczona wartość kwoty podanej w źródłowej jednostce monetarnej, na jednostkę docelową. " +
                            "\n3. Wygeneruj wykres – w celu skorzystania z tej funkcjonalności aplikacji, użytkownik musi określić liczbę walut (od 1 do 3), których notowania zostaną przedstawione na jednym wykresie. Kolejny krok polega na wyborze długości przedziału czasowego spośród dostępnych możliwości: 7, 14 lub 30 dni. Konieczny jest także wybór kolejnych jednostek monetarnych, a następnie zdefiniowanie końcowej daty przedziału czasowego notowań. Wygenerowany wykres będzie obejmował określoną liczbę dni wstecz, od daty końcowej. Istnieje możliwość przełączania się między wykresem słupkowym i liniowym.", "Pomoc", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }
    }
}
