﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace Graficznie.ViewModel
{
    public sealed class CurrencyCalculatorViewModel : BindableBase, INotifyPropertyChanged, IDataErrorInfo
    {
        private static CurrencyCalculatorViewModel m_oInstance = null;
        private static readonly object m_oPadLock = new object();
        private int m_nCounter = 0;

        public static CurrencyCalculatorViewModel Instance
        {
            get
            {
                lock (m_oPadLock)
                {
                    if (m_oInstance == null)
                    {
                        m_oInstance = new CurrencyCalculatorViewModel();
                    }
                    return m_oInstance;
                }
            }
        }

        private CurrencyCalculatorViewModel()
        {
            m_nCounter = 1;
        }

        string from = "PLN";
        string to = "USD";
        DateTime date = DateTime.Now;
        double amount = 100;

        string[] currencies = Currency.symbolsForCalculator;

        DateTime lowestDate = DateTime.ParseExact("1999-02-01", "yyyy-MM-dd", CultureInfo.CurrentCulture);

        public string From
        {
            get
            {
                return from;
            }
            set
            {
                from = value;
                OnPropertyChanged(null);
            }
        }
        public string To
        {
            get
            {
                return to;
            }
            set
            {
                to = value;
                OnPropertyChanged(null);
            }
        }
        public double Amount
        {
            get
            {
                if (amount < 0)
                {
                    Amount = 0;
                }
                return amount;
            }
            set
            {
                if (amount < 0)
                    amount = 0;
                else
                    amount = value;
                OnPropertyChanged(null);

            }
        }
        public DateTime Date
        {
            get
            {
                if (date > DateTime.Now)
                    Date = DateTime.Now;
                else if (date < lowestDate)
                    Date = lowestDate;
                return date;
            }
            set
            {
                if (date > DateTime.Now)
                    date = DateTime.Now;
                else
                    date = value;
                OnPropertyChanged(null);
            }
        }

        PriceInterface currencyPrice = new Price();

        public string Result
        {
            get
            {
                try
                {
                    double r = 0;
                    Task t = Task.Run(() =>
                    {
                        try
                        {
                            r = currencyPrice.Convert(From, To, Amount, Date);
                        }
                        catch
                        {
                            r = -100;
                        }
                    });
                    t.Wait(5000);
                    if (t.IsCompleted && r != -100)
                        return Math.Round(amount, 2).ToString() + " " + from + " = " + Math.Round(r, 2).ToString() + " " + to;
                    else
                        return "Nie udało się pobrać danych z bazy";
                }
                catch
                {
                    return "Nie udało się pobrać danych z bazy";
                }
            }
        }

        public string[] Currencies
        {
            get
            {
                return currencies;
            }
        }

        public string Error { get { throw new NotImplementedException(); } }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Date")
                {
                    if (Date < DateTime.ParseExact("1999-02-01", "yyyy-MM-dd", CultureInfo.CurrentCulture))
                        return "Za stara data";
                    else if (Date > DateTime.Now)
                        return "Za nowa data";
                }
                if (columnName == "Amount")
                {
                    if (amount < 0)
                        return "Za stara data";
                    else if (Date > DateTime.Now)
                        return "Za nowa data";
                }
                return null;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }
    }
}


