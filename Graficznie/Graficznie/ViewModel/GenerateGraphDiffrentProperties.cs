﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graficznie.ViewModel
{
    class GenerateGraphDiffrentProperties : BindableBase, INotifyPropertyChanged, IDataErrorInfo
    {
        //public static DataToGraph data = new DataToGraph();

        public int dateIsValid = 0;

        string saveSecondCurrency = "EUR";
        string saveThirdCurrency = "GBP";

        DateTime lowestDate = DateTime.ParseExact("1999-02-01", "yyyy-MM-dd", CultureInfo.CurrentCulture);
        DateTime date;
        string first;
        string second;
        string third;

        public string First
        {
            get
            {
                return first;
            }
            set
            {
                first = value;
                OnPropertyChanged(null);
            }
        }

        public string Second
        {
            get
            {
                return second;
            }
            set
            {
                second = value;
                OnPropertyChanged(null);
            }
        }

        public string Third
        {
            get
            {
                return third;
            }
            set
            {
                third = value;
                OnPropertyChanged(null);
            }
        }
        public DateTime Date
        {
            get
            {
                if (date > DateTime.Now)
                    Date = DateTime.Now;
                else if (date < lowestDate)
                    Date = lowestDate;
                return date;
            }
            set
            {
                if (date > DateTime.Now)
                    date = DateTime.Now;
                else
                    date = value;
                OnPropertyChanged(null);
            }
        }

        public string Error { get { throw new NotImplementedException(); } }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Date")
                {
                    if (Date < DateTime.ParseExact("1999-02-01", "yyyy-MM-dd", CultureInfo.CurrentCulture))
                    {
                        return "Za stara data";
                    }
                    else if (Date > DateTime.Now)
                    {
                        return "Za nowa data";
                    }
                }
                if (columnName == "First")
                {
                    if (First == Second || First == Third)
                    {
                        return "Error";
                    }
                }
                if (columnName == "Second")
                {
                    if (Second != null && (First == Second || Second == Third))
                    {
                        return "Error";
                    }
                }
                if (columnName == "Third")
                {
                    if (Third != null && (Third == Second || First == Third))
                    {
                        return "Error";
                    }
                }
                return null;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }
    }

    /*List<SpecificCurrencyUntilDate> list1 = null;
    List<SpecificCurrencyUntilDate> list2 = null;
    List<SpecificCurrencyUntilDate> list3 = null;
    DateTime chooseDate;
    int amountCurrency = 1;
    int amountDays = 7;

    async Task Progress()
    {
        var progress = new Progress<int>(value => loadCurrencyExchangeProgressBar.Value = value);
        await Task.Run(() =>
        {
            for (int i = 0; i < 100; i++)
            {
                ((IProgress<int>)progress).Report(i);
                Thread.Sleep(30);
            }
        });
    }

    async Task getData()
    {
        Dictionary<DateTime, Currency.CurrencyPrice> list11 = null;
        Dictionary<DateTime, Currency.CurrencyPrice> list22 = null;
        Dictionary<DateTime, Currency.CurrencyPrice> list33 = null;
        //You need find an async API for file copy, and System.IO has a lot to offer.
        //Also, there is no reason to create a Task for MyAsyncFileCopyMethod - the UI
        // will not wait (blocked) for the operation to complete if you use await:   

        list11 = await Currency.getPricesUntilDateToGraphAsync(data.First, chooseDate, amountDays);
        if (amountCurrency == 2)
            list22 = await Currency.getPricesUntilDateToGraphAsync(data.Second, chooseDate, amountDays);
        else if (amountCurrency == 3)
        {
            list22 = await Currency.getPricesUntilDateToGraphAsync(data.Second, chooseDate, amountDays);
            list33 = await Currency.getPricesUntilDateToGraphAsync(data.Third, chooseDate, amountDays);
        }
        if (amountCurrency == 1 && list11 != null)
        {
            list1 = new List<SpecificCurrencyUntilDate>();
            for (int i = 0; i < list11.Count; i++)
            {
                DateTime currentDate = chooseDate.AddDays(-i);
                SpecificCurrencyUntilDate s = new SpecificCurrencyUntilDate();
                s.Date = currentDate;
                s.Value = list11[currentDate].Value;
                list1.Add(s);
            }

            double min = list1[0].Value, max = list1[0].Value;

            for (int ii = 0; ii < list1.Count; ii++)
            {
                if (min > list1[ii].Value)
                    min = list1[ii].Value;
                if (max < list1[ii].Value)
                    max = list1[ii].Value;
            }

            double delta = (max - min) / (list1.Count - 1);

            CategoricalAxis catAxis = new CategoricalAxis();
            LinearAxis lineAxis = new LinearAxis();
            //lineAxis.Maximum = max + 0.2;
            //lineAxis.Minimum = min - 0.3;
            currencyChart.HorizontalAxis = catAxis;
            currencyChart.VerticalAxis = lineAxis;

            BarSeries barSeries = new BarSeries();
            Style style1 = new Style(typeof(Border));
            style1.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.Crimson)));
            barSeries.DefaultVisualStyle = style1;

            for (int i = list1.Count - 1; i >= 0; i--)
                barSeries.DataPoints.Add(new CategoricalDataPoint() { Category = list1[i].Date.ToString("dd") + "\n" + list1[i].Date.ToString("MM") + "\n" + list1[i].Date.ToString("yy"), Value = list1[i].Value });
            currencyChart.Series.Add(barSeries);
        }
        else if (amountCurrency == 2 && list11 != null && list22 != null)
        {
            list1 = new List<SpecificCurrencyUntilDate>();
            list2 = new List<SpecificCurrencyUntilDate>();
            for (int i = 0; i < list11.Count; i++)
            {
                DateTime currentDate = chooseDate.AddDays(-i);
                SpecificCurrencyUntilDate s = new SpecificCurrencyUntilDate();
                s.Date = currentDate;
                s.Value = list11[currentDate].Value;
                list1.Add(s);
                SpecificCurrencyUntilDate s2 = new SpecificCurrencyUntilDate();
                s2.Date = currentDate;
                s2.Value = list22[currentDate].Value;
                list2.Add(s2);
            }

            double min = list1[0].Value, max = list1[0].Value;

            for (int ii = 0; ii < list1.Count; ii++)
            {
                if (min > list1[ii].Value)
                    min = list1[ii].Value;
                if (list2 != null && min > list2[ii].Value)
                    min = list2[ii].Value;
                if (max < list1[ii].Value)
                    max = list1[ii].Value;
                if (list2 != null && max < list2[ii].Value)
                    min = list2[ii].Value;
            }

            CategoricalAxis catAxis = new CategoricalAxis();
            LinearAxis lineAxis = new LinearAxis();
            //lineAxis.Maximum = max + 0.5;
            //lineAxis.Minimum = min - 0.3;
            currencyChart.HorizontalAxis = catAxis;
            currencyChart.VerticalAxis = lineAxis;

            BarSeries barSeries1 = new BarSeries();
            Style style1 = new Style(typeof(Border));
            style1.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.Crimson)));
            barSeries1.DefaultVisualStyle = style1;

            BarSeries barSeries2 = new BarSeries();
            Style style2 = new Style(typeof(Border));
            style2.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
            barSeries2.DefaultVisualStyle = style2;

            for (int i = list1.Count - 1; i >= 0; i--)
            {
                barSeries1.DataPoints.Add(new CategoricalDataPoint() { Category = list1[i].Date.ToString("dd") + "\n" + list1[i].Date.ToString("MM") + "\n" + list1[i].Date.ToString("yy"), Value = list1[i].Value });
                barSeries2.DataPoints.Add(new CategoricalDataPoint() { Category = list2[i].Date.ToString("dd") + "\n" + list2[i].Date.ToString("MM") + "\n" + list2[i].Date.ToString("yy"), Value = list2[i].Value });
            }
            currencyChart.Series.Add(barSeries1);
            currencyChart.Series.Add(barSeries2);
        }
        else if (amountCurrency == 3 && list11 != null && list22 != null && list33 != null)
        {
            list1 = new List<SpecificCurrencyUntilDate>();
            list2 = new List<SpecificCurrencyUntilDate>();
            list3 = new List<SpecificCurrencyUntilDate>();
            for (int i = 0; i < list11.Count; i++)
            {
                DateTime currentDate = chooseDate.AddDays(-i);
                SpecificCurrencyUntilDate s = new SpecificCurrencyUntilDate();
                s.Date = currentDate;
                s.Value = list11[currentDate].Value;
                list1.Add(s);
                SpecificCurrencyUntilDate s2 = new SpecificCurrencyUntilDate();
                s2.Date = currentDate;
                s2.Value = list22[currentDate].Value;
                list2.Add(s2);
                SpecificCurrencyUntilDate s3 = new SpecificCurrencyUntilDate();
                s3.Date = currentDate;
                s3.Value = list33[currentDate].Value;
                list3.Add(s3);
            }

            double min = list1[0].Value, max = list1[0].Value;

            for (int ii = 0; ii < list1.Count; ii++)
            {
                if (min > list1[ii].Value)
                    min = list1[ii].Value;
                if (list2 != null && min > list2[ii].Value)
                    min = list2[ii].Value;
                if (list3 != null && min > list3[ii].Value)
                    min = list3[ii].Value;

                if (max < list1[ii].Value)
                    max = list1[ii].Value;
                if (list2 != null && max < list2[ii].Value)
                    max = list2[ii].Value;
                if (list3 != null && max < list3[ii].Value)
                    max = list3[ii].Value;
            }

            CategoricalAxis catAxis = new CategoricalAxis();
            LinearAxis lineAxis = new LinearAxis();
            //lineAxis.Maximum = max + 0.5;
            //lineAxis.Minimum = min - 0.3;
            currencyChart.HorizontalAxis = catAxis;
            currencyChart.VerticalAxis = lineAxis;

            BarSeries barSeries1 = new BarSeries();
            Style style1 = new Style(typeof(Border));
            style1.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.Crimson)));
            barSeries1.DefaultVisualStyle = style1;

            BarSeries barSeries2 = new BarSeries();
            Style style2 = new Style(typeof(Border));
            style2.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
            barSeries2.DefaultVisualStyle = style2;

            BarSeries barSeries3 = new BarSeries();
            Style style3 = new Style(typeof(Border));
            style3.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.GreenYellow)));
            barSeries3.DefaultVisualStyle = style3;

            //barSeries.Foreground = (Brush)ColorSelect.SelectedItem;
            for (int i = list1.Count - 1; i >= 0; i--)
            {
                barSeries1.DataPoints.Add(new CategoricalDataPoint() { Category = list1[i].Date.ToString("dd") + "\n" + list1[i].Date.ToString("MM") + "\n" + list1[i].Date.ToString("yy"), Value = list1[i].Value });
                barSeries2.DataPoints.Add(new CategoricalDataPoint() { Category = list2[i].Date.ToString("dd") + "\n" + list2[i].Date.ToString("MM") + "\n" + list2[i].Date.ToString("yy"), Value = list2[i].Value });
                barSeries3.DataPoints.Add(new CategoricalDataPoint() { Category = list3[i].Date.ToString("dd") + "\n" + list3[i].Date.ToString("MM") + "\n" + list3[i].Date.ToString("yy"), Value = list3[i].Value });
            }
            currencyChart.Series.Add(barSeries1);
            currencyChart.Series.Add(barSeries2);
            currencyChart.Series.Add(barSeries3);

        }
        else throw new Exception();
    }

    private void Validation_Error(object sender, ValidationErrorEventArgs e)
    {
        if (e.Action == ValidationErrorEventAction.Added)
            dateIsValid++;
        else
            dateIsValid--;
    }

    private void getData_CanExecute(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = dateIsValid == 0;
        e.Handled = true;
    }

    private async void getData_Executed(object sender, ExecutedRoutedEventArgs e)
    {
        CurrencyInformation.Visibility = Visibility.Visible;
        firstCurrencyLabel.Content = firstSymbolsComboBox.Text;
        if (amountCurrency == 2)
        {
            secondImage.Visibility = Visibility.Visible;
            secondCurrencyLabel.Visibility = Visibility.Visible;
            secondCurrencyLabel.Content = secondSymbolsComboBox.Text;
        }
        if (amountCurrency == 3)
        {
            secondImage.Visibility = Visibility.Visible;
            secondCurrencyLabel.Visibility = Visibility.Visible;
            secondCurrencyLabel.Content = secondSymbolsComboBox.Text;

            thirdImage.Visibility = Visibility.Visible;
            thirdCurrencyLabel.Visibility = Visibility.Visible;
            thirdCurrencyLabel.Content = thirdSymbolsComboBox.Text;
        }
        chooseDate = (DateTime)currencyDataPicker.SelectedValue;
        CurrencyOption.Visibility = Visibility.Collapsed;
        loadCurrencyExchangePanel.Visibility = Visibility.Visible;
        try
        {
            await getData();
            await Progress();
            loadCurrencyExchangePanel.Visibility = Visibility.Collapsed;
            currencyChart.Visibility = Visibility.Visible;
            DesignGraphOption.Visibility = Visibility.Visible;
            LegendText1.Text = firstSymbolsComboBox.Text;
            if (list2 != null)
            {
                LegendColor2.Visibility = Visibility.Visible;
                LegendText2.Visibility = Visibility.Visible;
                LegendText2.Text = secondSymbolsComboBox.Text;
            }
            if (list3 != null)
            {

                LegendColor2.Visibility = Visibility.Visible;
                LegendText2.Visibility = Visibility.Visible;
                LegendText2.Text = secondSymbolsComboBox.Text;

                LegendColor3.Visibility = Visibility.Visible;
                LegendText3.Visibility = Visibility.Visible;
                LegendText3.Text = thirdSymbolsComboBox.Text;
            }

        }
        catch
        {
            loadCurrencyExchangePanel.Margin = new Thickness(0, 0, 0, 0);
            StatusLabel.Foreground = Brushes.Red;
            StatusLabel.FontStyle = FontStyles.Italic;
            StatusLabel.Content = "Nie udało się pobrać danych z bazy";
            loadCurrencyExchangeProgressBar.Visibility = Visibility.Collapsed;
            currencyChart.Visibility = Visibility.Collapsed;
        }
        loadCurrencyExchangeProgressBar.Value = 0;
    }

    private void ChangeCurrenyAmount(object sender, RoutedEventArgs e)
    {
        if ((string)((RadRadioButton)sender).Content == "1" && (bool)((RadRadioButton)sender).IsChecked)
        {
            amountCurrency = 1;
            if (secondSymbolsComboBox.SelectedItem != null)
                saveSecondCurrency = (string)secondSymbolsComboBox.SelectedItem;
            if (thirdSymbolsComboBox.SelectedItem != null)
                saveThirdCurrency = (string)thirdSymbolsComboBox.SelectedItem;
            secondSymbols.Visibility = Visibility.Collapsed;
            thirdSymbols.Visibility = Visibility.Collapsed;
            thirdSymbolsComboBox.SelectedItem = null;
            secondSymbolsComboBox.SelectedItem = null;

        }
        else if ((string)((RadRadioButton)sender).Content == "2" && (bool)((RadRadioButton)sender).IsChecked)
        {
            amountCurrency = 2;
            if (thirdSymbolsComboBox.SelectedItem != null)
                saveThirdCurrency = (string)thirdSymbolsComboBox.SelectedItem;
            if (secondSymbolsComboBox.SelectedItem == null)
                secondSymbolsComboBox.SelectedItem = saveSecondCurrency;
            thirdSymbolsComboBox.SelectedItem = null;
            secondSymbols.Visibility = Visibility.Visible;
            thirdSymbols.Visibility = Visibility.Collapsed;
        }
        else
        {
            amountCurrency = 3;
            if (secondSymbolsComboBox.SelectedItem == null)
                secondSymbolsComboBox.SelectedItem = saveSecondCurrency;
            if (thirdSymbolsComboBox.SelectedItem == null)
                thirdSymbolsComboBox.SelectedItem = saveThirdCurrency;
            secondSymbols.Visibility = Visibility.Visible;
            thirdSymbols.Visibility = Visibility.Visible;
        }
    }

    private void ChangeAmount(object sender, RoutedEventArgs e)
    {
        if ((string)((RadRadioButton)sender).Content == "7" && (bool)((RadRadioButton)sender).IsChecked)
        {
            amountDays = 7;
        }
        else if ((string)((RadRadioButton)sender).Content == "14" && (bool)((RadRadioButton)sender).IsChecked)
        {
            amountDays = 14;
        }
        else
        {
            amountDays = 30;
        }
    }

    private void ChangeChart(object sender, RoutedEventArgs e)
    {
        if ((string)((RadRadioButton)sender).Content == "Słupkowy" && (bool)((RadRadioButton)sender).IsChecked)
        {
            for (int i = currencyChart.Series.Count - 1; i >= 0; i--)
                currencyChart.Series.RemoveAt(i);

            BarSeries barSeries1 = new BarSeries();
            Style style1 = new Style(typeof(Border));
            style1.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.Crimson)));
            barSeries1.DefaultVisualStyle = style1;

            BarSeries barSeries2 = new BarSeries();
            Style style2 = new Style(typeof(Border));
            style2.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.CornflowerBlue)));
            barSeries2.DefaultVisualStyle = style2;

            BarSeries barSeries3 = new BarSeries();
            Style style3 = new Style(typeof(Border));
            style3.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Colors.GreenYellow)));
            barSeries3.DefaultVisualStyle = style3;

            //barSeries.Foreground = (Brush)ColorSelect.SelectedItem;
            for (int i = list1.Count - 1; i >= 0; i--)
            {
                barSeries1.DataPoints.Add(new CategoricalDataPoint() { Category = list1[i].Date.ToString("dd") + "\n" + list1[i].Date.ToString("MM") + "\n" + list1[i].Date.ToString("yy"), Value = list1[i].Value });
                if (list2 != null)
                {
                    barSeries2.DataPoints.Add(new CategoricalDataPoint() { Category = list2[i].Date.ToString("dd") + "\n" + list2[i].Date.ToString("MM") + "\n" + list2[i].Date.ToString("yy"), Value = list2[i].Value });
                }
                if (list3 != null)
                {
                    barSeries3.DataPoints.Add(new CategoricalDataPoint() { Category = list3[i].Date.ToString("dd") + "\n" + list3[i].Date.ToString("MM") + "\n" + list3[i].Date.ToString("yy"), Value = list3[i].Value });
                }
            }
            currencyChart.Series.Add(barSeries1);
            if (list2 != null)
                currencyChart.Series.Add(barSeries2);
            if (list3 != null)
                currencyChart.Series.Add(barSeries3);

        }
        else
        {
            for (int i = currencyChart.Series.Count - 1; i >= 0; i--)
                currencyChart.Series.RemoveAt(i);

            LineSeries barSeries1 = new LineSeries();
            barSeries1.Stroke = new SolidColorBrush(Colors.Crimson);

            LineSeries barSeries2 = new LineSeries();
            barSeries2.Stroke = new SolidColorBrush(Colors.CornflowerBlue);

            LineSeries barSeries3 = new LineSeries();
            barSeries3.Stroke = new SolidColorBrush(Colors.GreenYellow);


            //barSeries.Foreground = (Brush)ColorSelect.SelectedItem;
            for (int i = list1.Count - 1; i >= 0; i--)
            {
                barSeries1.DataPoints.Add(new CategoricalDataPoint() { Category = list1[i].Date.ToString("dd") + "\n" + list1[i].Date.ToString("MM") + "\n" + list1[i].Date.ToString("yy"), Value = list1[i].Value });
                if (list2 != null)
                {
                    barSeries2.DataPoints.Add(new CategoricalDataPoint() { Category = list2[i].Date.ToString("dd") + "\n" + list2[i].Date.ToString("MM") + "\n" + list2[i].Date.ToString("yy"), Value = list2[i].Value });
                }
                if (list3 != null)
                {
                    barSeries3.DataPoints.Add(new CategoricalDataPoint() { Category = list3[i].Date.ToString("dd") + "\n" + list3[i].Date.ToString("MM") + "\n" + list3[i].Date.ToString("yy"), Value = list3[i].Value });
                }
            }
            currencyChart.Series.Add(barSeries1);
            if (list2 != null)
                currencyChart.Series.Add(barSeries2);
            if (list3 != null)
                currencyChart.Series.Add(barSeries3);

        }
    }
}*/
}
