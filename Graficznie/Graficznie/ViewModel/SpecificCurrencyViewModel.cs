﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Graficznie.ViewModel
{
    class SpecificCurrencyViewModel : BindableBase, INotifyPropertyChanged
    {
        CurrencyPrice specificCurrency = null;

        public CurrencyPrice SpecificCurrency
        {
            get
            {
                return specificCurrency;
            }
            set
            {
                specificCurrency = value;
                OnPropertyChanged(null);
            }
        }

        public string CurrencyText
        {
            get
            {
                    return SpecificCurrency.Name + " (" + Currency.fullNamesDictionary[SpecificCurrency.Name] + ") do dnia " + SpecificCurrency.Date.ToString("d.MM.yyyy");
            }
        }

        List<CurrencyPrice> list = null;

        public List<CurrencyPrice> List
        {
            get
            {
                return list;
            }
            set
            {
                list = value;
                OnPropertyChanged(null);
            }
        }

        double loadCurrencyExchangeProgressBar = 0;

        public double LoadCurrencyExchangeProgressBar
        {
            get
            {
                return loadCurrencyExchangeProgressBar;
            }
            set
            {
                loadCurrencyExchangeProgressBar = value;
                OnPropertyChanged(null);
            }
        }

        async Task Progress()
        {
            var progress = new Progress<int>(value => LoadCurrencyExchangeProgressBar = value);
            await Task.Run(() =>
            {
                for (int i = 0; i < 100; i++)
                {
                    ((IProgress<int>)progress).Report(i);
                    Thread.Sleep(30);
                }
            });
        }

        async Task getData()
        {
            List = null;
            //You need find an async API for file copy, and System.IO has a lot to offer.
            //Also, there is no reason to create a Task for MyAsyncFileCopyMethod - the UI
            // will not wait (blocked) for the operation to complete if you use await:
            Currency.Strategy = new DataToListStrategy();
            List = await Currency.getPricesStrategyAsync(SpecificCurrency.Name, SpecificCurrency.Date);
            if (List == null)
                throw new Exception();
        }

        Visibility currencyExchangeListBoxVisibility = Visibility.Collapsed;

        public Visibility CurrencyExchangeListBoxVisibility
        {
            get
            {
                return currencyExchangeListBoxVisibility;
            }
            set
            {
                currencyExchangeListBoxVisibility = value;
                OnPropertyChanged(null);
            }
        }

        Visibility generateGraphButtonVisibility = Visibility.Collapsed;

        public Visibility GenerateGraphButtonVisibility
        {
            get
            {
                return generateGraphButtonVisibility;
            }
            set
            {
                generateGraphButtonVisibility = value;
                OnPropertyChanged(null);
            }
        }

        Visibility loadCurrencyExchangePanelVisibility = Visibility.Visible;

        public Visibility LoadCurrencyExchangePanelVisibility
        {
            get
            {
                return loadCurrencyExchangePanelVisibility;
            }
            set
            {
                loadCurrencyExchangePanelVisibility = value;
                OnPropertyChanged(null);
            }
        }

        Visibility errorLoadCurrencyExchangePanelVisibility = Visibility.Collapsed;

        public Visibility ErrorLoadCurrencyExchangePanelVisibility
        {
            get
            {
                return errorLoadCurrencyExchangePanelVisibility;
            }
            set
            {
                errorLoadCurrencyExchangePanelVisibility = value;
                OnPropertyChanged(null);
            }
        }

        private async void LoadData()
        {
            try
            {
                ErrorLoadCurrencyExchangePanelVisibility = Visibility.Collapsed;
                await getData();
                await Progress();
                CurrencyExchangeListBoxVisibility = Visibility.Visible;
                GenerateGraphButtonVisibility = Visibility.Visible;
                LoadCurrencyExchangePanelVisibility = Visibility.Collapsed;
            }
            catch
            {
                ErrorLoadCurrencyExchangePanelVisibility = Visibility.Visible;
                CurrencyExchangeListBoxVisibility = Visibility.Collapsed;
                LoadCurrencyExchangePanelVisibility = Visibility.Collapsed;
                GenerateGraphButtonVisibility = Visibility.Collapsed;
            }
            loadCurrencyExchangeProgressBar = 0;
        }

        public ICommand ShowWindowCommand
        {
            get;
            internal set;
        }

        public SpecificCurrencyViewModel(object specificCurrency)
        {
            SpecificCurrency = specificCurrency as CurrencyPrice;
            LoadData();
            ShowWindowCommand = new DelegateCommand<object>(ShowWindowExecute);
        }

        private void ShowWindowExecute(object arg)
        {
            Factory.CreateWindow(arg).Show();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }
    }
}
