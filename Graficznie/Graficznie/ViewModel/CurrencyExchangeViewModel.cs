﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Prism.Mvvm;
using Prism.Commands;
using System.Windows.Input;
using System.Collections;
using System.Collections.ObjectModel;

namespace Graficznie.ViewModel
{
    class CurrencyExchangeViewModel : BindableBase, INotifyPropertyChanged, INotifyDataErrorInfo
    {
        ReadOnlyObservableCollection<ValidationError> errors;

        public ReadOnlyObservableCollection<ValidationError> Errors
        {
            get
            {
                return errors;
            }
            set
            {
                errors = value;
            }
        }

        private ErrorsContainer<string> errorsContainer;

        protected ErrorsContainer<string> ErrorsContainer
        {
            get
            {
                if (this.errorsContainer == null)
                {
                    this.errorsContainer =
                        new ErrorsContainer<string>(pn => this.RaiseErrorsChanged(pn));
                }

                return this.errorsContainer;
            }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public bool HasErrors
        {
            get { return this.ErrorsContainer.HasErrors; }
        }

        public IEnumerable GetErrors(string propertyName)
        {
            return this.errorsContainer.GetErrors(propertyName);
        }

        protected void RaiseErrorsChanged(string propertyName)
        {
            var handler = this.ErrorsChanged;
            if (handler != null)
            {
                handler(this, new DataErrorsChangedEventArgs(propertyName));
            }
        }

        List<CurrencyPrice> list = null;

        public List<CurrencyPrice> List
        {
            get
            {
                return list;
            }
            set
            {
                list = value;
                OnPropertyChanged(null);
            }
        }


        public CurrencyExchangeViewModel()
        {
            GetCurrenciesCommand = new DelegateCommand<object>(GetCurrenciesExecute, CanExecuteGetCurrenciesCommand);
            SelectCurrencyCommand = new DelegateCommand<object>(SelectCurrencyExecute);
        }

        double loadCurrencyExchangeProgressBar = 0;

        public double LoadCurrencyExchangeProgressBar
        {
            get
            {
                return loadCurrencyExchangeProgressBar;
            }
            set
            {
                loadCurrencyExchangeProgressBar = value;
                OnPropertyChanged(null);
            }
        }

        public ICommand GetCurrenciesCommand
        {
            get;
            internal set;
        }

        private bool CanExecuteGetCurrenciesCommand(object arg)
        {
            return !HasErrors;
        }

        Visibility selectDatePanelVisibility = Visibility.Visible;

        public Visibility SelectDatePanelVisibility
        {
            get
            {
                return selectDatePanelVisibility;
            }
            set
            {
                selectDatePanelVisibility = value;
                OnPropertyChanged(null);
            }
        }

        Visibility currencyDateShowVisibility = Visibility.Collapsed;

        public Visibility CurrencyDateShowVisibility
        {
            get
            {
                return currencyDateShowVisibility;
            }
            set
            {
                currencyDateShowVisibility = value;
                OnPropertyChanged(null);
            }
        }

        Visibility errorLoadCurrencyExchangePanelVisibility = Visibility.Collapsed;

        public Visibility ErrorLoadCurrencyExchangePanelVisibility
        {
            get
            {
                return errorLoadCurrencyExchangePanelVisibility;
            }
            set
            {
                errorLoadCurrencyExchangePanelVisibility = value;
                OnPropertyChanged(null);
            }

        }

        Visibility loadCurrencyExchangePanelVisibility = Visibility.Collapsed;

        public Visibility LoadCurrencyExchangePanelVisibility
        {
            get
            {
                return loadCurrencyExchangePanelVisibility;
            }
            set
            {
                loadCurrencyExchangePanelVisibility = value;
                OnPropertyChanged(null);
            }

        }

        public string CurrencyDateText
        {
            get
            {
                return "Data waluty: " + Date.ToString("d.MM.yyyy");
            }
        }

        Visibility currencyExchangeListBoxVisibility = Visibility.Collapsed;

        public Visibility CurrencyExchangeListBoxVisibility
        {
            get
            {
                return currencyExchangeListBoxVisibility;
            }
            set
            {
                currencyExchangeListBoxVisibility = value;
                OnPropertyChanged(null);
            }
        }

        private async void GetCurrenciesExecute(object arg)
        {
            ErrorLoadCurrencyExchangePanelVisibility = Visibility.Collapsed;
            CurrencyDateShowVisibility = Visibility.Collapsed;
            LoadCurrencyExchangePanelVisibility = Visibility.Visible;
            CurrencyExchangeListBoxVisibility = Visibility.Collapsed;
            SelectDatePanelVisibility = Visibility.Collapsed;
            try
            {
                await getData();
                await Progress();
                CurrencyDateShowVisibility = Visibility.Visible;               
                CurrencyExchangeListBoxVisibility = Visibility.Visible;
                LoadCurrencyExchangePanelVisibility = Visibility.Collapsed;
            }
            catch
            {
                ErrorLoadCurrencyExchangePanelVisibility = Visibility.Visible;
                CurrencyExchangeListBoxVisibility = Visibility.Collapsed;                
            }
            LoadCurrencyExchangePanelVisibility = Visibility.Collapsed;
            SelectDatePanelVisibility = Visibility.Visible;
            LoadCurrencyExchangeProgressBar = 0;
        }

        private async Task Progress()
        {
            var progress = new Progress<int>(value => loadCurrencyExchangeProgressBar = value);
            await Task.Run(() =>
            {
                for (var i = 0; i < 100; i++)
                {
                    ((IProgress<int>)progress).Report(i);
                    Thread.Sleep(30);
                }
            });
        }

        private async Task getData()
        {
            List = null;
            //You need find an async API for file copy, and System.IO has a lot to offer.
            //Also, there is no reason to create a Task for MyAsyncFileCopyMethod - the UI
            // will not wait (blocked) for the operation to complete if you use await:   
            List = await Currency.getPricesOnDateAsync(Date);
            if (List == null)
                throw new Exception();
        }

        private DateTime date = DateTime.Now;

        private readonly DateTime lowestDate =
            DateTime.ParseExact("1999-02-01", "yyyy-MM-dd", CultureInfo.CurrentCulture);


        public DateTime Date
        {
            get
            {
                if (date > DateTime.Now)
                    Date = DateTime.Now;
                else if (date < lowestDate)
                    Date = lowestDate;
                return date;
            }
            set
            {
                if (date > DateTime.Now)
                    date = DateTime.Now;
                else
                    date = value;
                OnPropertyChanged(null);
            }
        }
        public ICommand SelectCurrencyCommand
        {
            get;
            internal set;
        }

        private void SelectCurrencyExecute(object arg)
        {
            Factory.CreateWindow(arg).Show();
        }

        /*public string Error => throw new NotImplementedException();

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Date")
                    if (Date < DateTime.ParseExact("1999-02-01", "yyyy-MM-dd", CultureInfo.CurrentCulture))
                        return "Za stara data";
                    else if (Date > DateTime.Now)
                        return "Za nowa data";
                return null;
            }
        }*/

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                    new PropertyChangedEventArgs(property));
        }
    }
}
