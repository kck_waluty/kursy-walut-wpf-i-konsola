﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Telerik.Charting;
using Telerik.Windows.Controls.ChartView;

namespace Graficznie.ViewModel
{
    class GenerateGraphViewModel : INotifyPropertyChanged
    {
        List<CurrencyPrice> list;

        public List<CurrencyPrice> List
        {
            get
            {
                return list;
            }
            set
            {
                list = value;
                OnPropertyChanged(null);
            }
        }

        Visibility barSeriesVisibility = Visibility.Visible;

        public Visibility BarSeriesVisibility
        {
            get
            {
                return barSeriesVisibility;
            }
            set
            {
                barSeriesVisibility = value;
                OnPropertyChanged(null);
            }
        }

        Visibility lineSeriesVisibility = Visibility.Collapsed;

        public Visibility LineSeriesVisibility
        {
            get
            {
                return lineSeriesVisibility;
            }
            set
            {
                lineSeriesVisibility = value;
                OnPropertyChanged(null);
            }
        }

        public string CurrencyText
        {
            get
            {
                return this.list[0].Name + " (" + Currency.fullNamesDictionary[this.list[0].Name] + ") do dnia " + this.list[0].Date.ToString("d.MM.yyyy");
            }
        }


        public GenerateGraphViewModel(object list)
        {
            List = list as List<CurrencyPrice>;

            ChangeGraphTypeCommand = new DelegateCommand<object>(ChangeGraphTypeExecute);


        }

        public ICommand ChangeGraphTypeCommand
        {
            get;
            internal set;
        }


        private void ChangeGraphTypeExecute(object arg)
        {
            if (arg.Equals("Słupkowy"))
            {
                BarSeriesVisibility = Visibility.Visible;
                LineSeriesVisibility = Visibility.Collapsed;
            }
            else if (arg.Equals("Liniowy"))
            {
                BarSeriesVisibility = Visibility.Collapsed;
                LineSeriesVisibility = Visibility.Visible;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }
    }
}
