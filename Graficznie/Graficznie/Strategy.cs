﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Graficznie
{
    interface IStrategy
    {
        List<CurrencyPrice> createData(string currency, DateTime dateTo, int numberOfDaysBack = 7);
    }
    class DataToListStrategy : IStrategy
    {
        PriceInterface currencyPrice = new ProxyPrice();
        public List<CurrencyPrice> createData(string currency, DateTime dateTo, int numberOfDaysBack = 7)
        {
            List<CurrencyPrice> list = new List<CurrencyPrice>();

            for (int i = 0; i >= -numberOfDaysBack; i--)
            {
                try
                {
                    DateTime date = dateTo.AddDays(i);
                    double price = currencyPrice.Convert(currency, "PLN", 1, date);
                    if (double.IsInfinity(price))
                    {
                        list.Add(new CurrencyPrice(currency, 0, date, 0, 0));
                        continue;
                    }
                    double price7DaysEarlier = currencyPrice.Convert(currency, "PLN", 1, date.AddDays(-7));
                    double percentDifference = (price - price7DaysEarlier) / price7DaysEarlier * 100;

                    //percentDifference = Math.Round(percentDifference, 3);

                    list.Add(new CurrencyPrice(currency, price, date, percentDifference, price - price7DaysEarlier));
                }
                catch
                {
                    return null;
                }
            }
            return list;
        }
    }

    class DataToGraphStrategy : IStrategy
    {
        PriceInterface currencyPrice = new ProxyPrice();
        public List<CurrencyPrice> createData(string currency, DateTime dateTo, int numberOfDaysBack = 7)
        {
            List<CurrencyPrice> list = new List<CurrencyPrice>();

            for (int i = 0; i >= -numberOfDaysBack; i--)
            {
                try
                {
                    DateTime date = dateTo.AddDays(i);
                    double price = currencyPrice.Convert(currency, "PLN", 1, date);
                    if (double.IsInfinity(price))
                    {
                        list.Add(new CurrencyPrice(currency, 0, date, 0, 0));
                        continue;
                    }

                    price = Math.Round(price, 3);

                    list.Add(new CurrencyPrice(currency, price, date));
                }
                catch
                {
                    return null;
                }
            }
            return list;
        }
    }
}
